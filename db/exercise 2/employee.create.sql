CREATE TABLE `training_db`.`employee` (
  `emp_id` INT NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NULL,
  `dob` DATE NOT NULL,
  `date_of_joining` DATE NOT NULL,
  `annual_salary` INT NOT NULL);


