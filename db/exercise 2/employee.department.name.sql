SELECT employee.first_name 
      ,employee.surname
      ,department.dept_name 
  FROM training_db.employee employee
 INNER JOIN training_db.department department
 WHERE employee.dept_id=department.dept_id;
