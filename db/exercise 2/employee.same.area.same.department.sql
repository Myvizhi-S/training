SELECT employee.first_name
      ,department.dept_name 
      ,employee.location
  FROM employee employee
      ,department department 
 WHERE employee.dept_id=department.dept_id 
   AND employee.location IN('erode')
   AND department.dept_name IN('ITDESK') 
 ORDER BY department.dept_name;
