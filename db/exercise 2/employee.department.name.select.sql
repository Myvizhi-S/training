SELECT e.first_name
      ,e.surname
      ,d.dept_name
  FROM employee e
      ,department d
 WHERE e.dept_id = d.dept_id;