SELECT employee.emp_id
      ,employee.first_name
      ,employee.surname
      ,employee.dob
      ,employee.date_of_joining
      ,employee.annual_salary
      ,employee.dept_id
      ,employee.location
  FROM employee
 WHERE DATE_FORMAT(dob,'%d-%m') = DATE_FORMAT(SYSDATE(),'%d-%m');