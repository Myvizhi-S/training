SELECT sch_ool.id
      ,sch_ool.school_name
      ,sch_ool.location
      ,sch_ool.students_count
      ,sch_ool.staff_count
      ,sch_ool.student_id
      ,sch_ool.staff_id
  FROM dbms.sch_ool