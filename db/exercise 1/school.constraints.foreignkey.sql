ALTER TABLE `dbms`.`sch_ool` 
  ADD CONSTRAINT `id`
  FOREIGN KEY (`id`)
  REFERENCES `dbms`.`staff` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
