CREATE DATABASE dbms;
CREATE TABLE dbms.school (
             school_name VARCHAR(50)
            ,place VARCHAR(25)
            ,students_count INT
            ,staff_count INT );
             
SHOW COLUMNS FROM dbms.school