SELECT id AS STUDENT_ID
	  ,roll_number AS ROLL_NO
      ,name AS STUDENT_NAME
      ,gender AS GENDER
      ,semester AS SEMESTER
      ,grade AS GRADE
      ,credits AS CREDITS
      ,gpa
  FROM student student
  LEFT JOIN semester_result semester_result
    ON student.id = semester_result.stud_id
 WHERE GPA >5
   AND semester IN (4,6,8)
 ORDER BY stud_id;