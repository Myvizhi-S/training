SELECT student.roll_number
      ,student.name
      ,student.gender
      ,student.dob
      ,student.email
      ,student.phone
      ,student.address
      ,college.name
      ,department.dept_name
      ,employee.name
  FROM student student
  LEFT JOIN college_department college_department
    ON student.cdept_id = college_department.cdept_id
  LEFT JOIN college college
    ON student.college_id = college.id
 INNER JOIN department department
    ON college_department.udept_code = department.dept_code
 INNER JOIN employee employee
    ON college.id = employee.college_id
 WHERE desig_id = '3'
 GROUP BY roll_number
 LIMIT 20