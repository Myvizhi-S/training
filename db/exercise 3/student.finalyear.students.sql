SELECT student.roll_number
      ,student.name
      ,student.gender
      ,student.dob
      ,student.email
      ,student.phone
      ,student.address
      ,student.academic_year AS Batch
      ,college.name
      ,department.dept_name
  FROM university
  LEFT JOIN college college
    ON university.univ_code = college.univ_code
  LEFT JOIN student student
    ON student.college_id = college.id
  LEFT JOIN college_department college_department
    ON student.cdept_id = college_department.cdept_id
 RIGHT JOIN department department
    ON college_department.udept_code = department.dept_code
 WHERE student.cdept_id = college_department.cdept_id
   AND university.university_name = 'anna university'
   AND college.city = 'coimbatore'
   AND student.academic_year = 2020