SELECT college.code
      ,college.name AS college_name
      ,university.university_name 
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.dept_name AS department_name
      ,employee.name AS hod_name
  FROM college college
  LEFT JOIN university university
    ON college.univ_code = university.univ_code
 RIGHT JOIN department department
    ON university.univ_code = department.univ_code
  LEFT JOIN employee employee 
    ON college.id = employee.college_id
  LEFT JOIN designation designation
    ON designation.id = employee.desig_id
 WHERE designation.name = "HOD"
HAVING department.dept_name IN ('CSE','IT');