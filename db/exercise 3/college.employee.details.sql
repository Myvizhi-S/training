SELECT university.univ_code
      ,college.name
      ,university.university_name
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.dept_code
      ,employee.name AS employee_name
      ,designation.name AS designation_name
      ,designation.rank AS employee_rank
  FROM employee employee
  LEFT JOIN college college
    ON employee.college_id = college.id
  LEFT JOIN college_department
    ON employee.cdept_id = college_department.cdept_id
  LEFT JOIN department department
    ON college_department.udept_code = department.dept_code
  LEFT JOIN designation designation
    ON employee.desig_id = designation.id
 ORDER BY designation.rank
         ,college.name;