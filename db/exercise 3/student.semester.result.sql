  SELECT u.university_name
        ,s.id AS roll_number
        ,s.name AS student_name
        ,s.gender
        ,s.dob
        ,s.address
        ,c.name AS college_name
        ,d.dept_name
        ,sr.semester
        ,sr.grade
        ,sr.credits
    FROM university u
        ,college c
        ,department d 
        ,designation de
        ,college_department cd
        ,employee e 
        ,student s
        ,syllabus sy
        ,professor_syllabus ps
        ,semester_result sr
   WHERE c.univ_code = u.univ_code 
     AND u.univ_code = d.univ_code 
     AND cd.college_id = c.id 
     AND cd.udept_code = d.dept_code
     AND e.college_id = c.id 
     AND e.cdept_id = cd.cdept_id 
     AND e.desig_id = de.id 
     AND s.college_id = c.id
     AND s.cdept_id = cd.cdept_id
     AND sy.cdept_id = cd.cdept_id
     AND ps.syllabus_id = sy.id
     AND sr.syllabus_id = sy.id
     AND sr.stud_id = s.id
   ORDER BY sr.semester AND c.name;