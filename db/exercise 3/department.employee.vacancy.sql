SELECT designation.name AS designation_name 
      ,designation.rank
      ,university.univ_code
      ,college.name AS college_name
      ,department.dept_name
      ,university.university_name
      ,college.city,c.state
      ,college.year_opened
  FROM professor_syllabus professor_syllabus
 INNER JOIN syllabus syllabus
    ON professor_syllabus.syllabus_id = syllabus.id
 INNER JOIN college_department college_department
    ON syllabus.cdept_id = college_department.cdept_id
 INNER JOIN department department
    ON college_department.udept_code = department.dept_code
 INNER JOIN employee employee
    ON college_department.college_id = employee.college_id
 INNER JOIN designation designation
    ON employee.desig_id = designation.id
 INNER JOIN college college
    ON college_department.college_id = college.college_id
 INNER JOIN university university
    ON college.univ_code = university.univ_code
 WHERE emp_id IS NULL
 GROUP BY syllabus_id