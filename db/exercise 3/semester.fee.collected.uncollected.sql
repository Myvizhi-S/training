 SELECT university.university_name
       ,syllabus.id AS roll_number
       ,syllabus.name AS student_name
       ,syllabus.gender
       ,syllabus.dob
       ,syllabus.address
       ,college.name AS college_name
       ,department.dept_name
       ,semester_fee.amount
       ,semester_fee.paid_year
       ,semester_fee.paid_status
   FROM semester_fee semester_fee
   LEFT JOIN student student
     ON semester_fee.stud_id = student.id
   LEFT JOIN college college
     ON student.college_id = college.id
  INNER JOIN university university
     ON college.univ_code = university.univ_code
  WHERE semester_fee.paid_status = 'paid'
  ORDER BY student.id;
 
 SELECT university.university_name
       ,syllabus.id AS roll_number
       ,syllabus.name AS student_name
       ,syllabus.gender
       ,syllabus.dob
       ,syllabus.address
       ,college.name AS college_name
       ,department.dept_name
       ,semester_fee.amount
       ,semester_fee.paid_year
       ,semester_fee.paid_status
   FROM semester_fee semester_fee
   LEFT JOIN student student
     ON semester_fee.stud_id = student.id
   LEFT JOIN college college
     ON student.college_id = college.id
  INNER JOIN university university
     ON college.univ_code = university.univ_code
  WHERE semester_fee.paid_status = 'unpaid'
  ORDER BY student.id;
 
 
 SELECT college_name
       ,university_name
       ,semester
       ,paid_year
       ,paid_status
       ,SUM(amount)
  FROM college_department college_department
 INNER JOIN semester_fee semester_fee
    ON college_department.cdept_id = semester_fee.cdept_id
 INNER JOIN college college
    ON college_department.college_id = college.id
 INNER JOIN university university
    ON college.univ_code = university.univ_code
 WHERE paid_status = 'paid'
   AND paid_year = 2020;