package training.java.module;
import java.util.Scanner;
public class MassConverter {
    private float kilo;
    private float pound;
    public MassConverter(float kilo,float pound) {
        this.kilo = kilo;
        this.pound = pound;
    }
    public void printMass() {
        System.out.println("The Mass in Kilogram is"+this.kilo);
        System.out.println("The Mass in Pound is"+this.pound);
    }
    public void kiloConverter() {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the Mass in Pound");
        this.pound=sc.nextFloat();
        this.kilo=this.pound*0.453592f;
    }
}