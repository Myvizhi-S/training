import java.util.Scanner;
public class PoundToKilogram {
    public void functionCall() {
        MassConverter convertPound = new MassConverter(0.0f,0.0f);
        convertPound.kiloConverter();
        convertPound.printMass();
    }
    class MassConverter {
        private float kilo;
        private float pound;
        public MassConverter(float kilo,float pound) {
            this.kilo = kilo;
            this.pound = pound;
        }
         public void printMass() {
            System.out.println("The Mass in Kilogram is"+this.kilo);
            System.out.println("The Mass in Pound is"+this.pound);
        }
        public void kiloConverter() {
            Scanner sc=new Scanner(System.in);
            System.out.println("Enter the Mass in Pound");
            this.pound=sc.nextFloat();
            this.kilo=this.pound*0.453592f;
        }
    }
    public static void main(String[] args) {
            System.out.println("MassConvertor");
            PoundToKilogram unit = new PoundToKilogram();
            unit.functionCall();
    }
}