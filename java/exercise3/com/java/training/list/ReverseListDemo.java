package com.java.training.list;
/*
 Requirement: 
   To reverse List Using Stack with minimum 7 elements in list.
 
 Entity: 
   public class ReverseListDemo.
   
 Function Declaration : 
   public static void main(String[] args)
 
 Jobs To Be Done: 
 1) Importing the arraylist and stack package. 
 2) Creating the list object as generic type. 
 3) Appending the list values.
 4) Creating the stack object in generic type. 
 5) Pushing each element of the list to the stack.
 6) Clearing the list elements to append the reversed list. 
 7) Using for loop,adding the popped element from the stack. 
 8) printing the reversed Stack list
 
 */

import java.util.ArrayList;
import java.util.Stack;

public class ReverseListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> integer = new ArrayList<>();
        integer.add(55);
        integer.add(70);
        integer.add(98);
        integer.add(100);
        integer.add(139);
        integer.add(25);
        integer.add(12);
        integer.add(200);
        System.out.println("Before reversing the list" + integer);
        
        Stack<Integer> stack = new Stack<>();
        for (Integer number : integer) {
            stack.push(number);
        }
        
        integer.clear();
        int size = stack.size();
        for (int iteration = 0; iteration < size; iteration++) {
            integer.add(stack.pop());
        }
        
        System.out.println("After reversing the list" + integer);
    }
}