package com.java.training.nio;

/*
Requirements:
      To create two paths and test whether they represent same path.
     
Entities:
      CheckPaths
      
Method Signature:
      public static void main(String[] args);
      
Jobs To Done:
      1.Get the two different paths as path1 and path2.
      2.Check whether the paths represents the same path.
 
Pseudo code:
class CheckPaths {
 
    public static void main(String[] args) {
        Path path1 = Paths.get("C:\\1 Dev\\training1\\java\\exercise1");
 		Path path2 = Paths.get("c:\\1 dev\\training1\\java\\exercise1");
 		System.out.println(path1.equals(path2));
    }
 }
 */

import java.nio.file.Path;
import java.nio.file.Paths;

public class CheckPathDemo {

	public static void main(String[] args) {
		Path path1 = Paths.get("C:\\nio\\myvizhi.txt");
		Path path2 = Paths.get("c:\\1 dev\\training1\\java\\exercise1");
		System.out.println(path1.equals(path2));
	}
}
