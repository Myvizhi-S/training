package com.java.training.nio;

/*
Requirements : 
		Read a file using java.nio.Files using Paths.


Entities :
  NioFiles

Method Signature :
	public static void main(String[] args)

Jobs To Be Done:
	1)Create a reference for Paths.
	2)Add all Lines in the file to the list.
	3)for each file name in the list.
		3.1)Print all lines.

PseudoCode:

class NioFiles {

	public static void main(String[] args) throws IOException {
			Path path = Paths.get("C:\\io\\text.txt");
			List<String> lines = Files.readAllLines(path);

			for (String line : lines) {
				System.out.println(line);
			}
	}
}
*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class NioFileDemo {

	public static void main(String[] args) throws IOException {
		Path path = Paths.get("C:\\nio\\myvizhi.txt");
		List<String> lines = Files.readAllLines(path);
		
		for (String line : lines) {
			System.out.println(line);
		}
	}

}