package com.java.training.generics;

/*
Requirements : 
    To write a generic method to count the number of elements in a collection that have a specific
    property (for example, odd integers, prime numbers, palindromes).

Entities :sd
    public class GenericMethodDemo
  
Function Declaration :
    public static void main(String[] args)
  
Jobs To Be Done:
    1) Importing the ArrayList.
    2) Creating the count method which returns the count of odd numbers present in a list.
    3) Creating the main method and create a list reference.
    4) Adding elements inside a list.
    5) Calling a count method and printing the number of odd numbers.
*/

import java.util.ArrayList;

public class GenericMethodDemo {
    
    public static  int countOddNumbers(ArrayList<Integer> list) {
        int value = 0;
        for (int elements : list) {
            if (elements % 2 != 0) {
                value++;
            }
        }
        
        return value;
    }
    
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(1);
        list.add(7);
        list.add(4);
        list.add(6);
        list.add(5);
        list.add(9);
        list.add(8);
        System.out.println("the count of odd number is " + countOddNumbers(list));
    }
}