/*
Requirement:
    To tell whether the following code will compile or not.
    public final class Algorithm {
        public static <T> T max(T x, T y) {
            return x > y ? x : y;
        }
    }

Entity:
    There is no classes have been used.

Function Declaration:
    There is no function is used.

Jobs to be done:
    To answer the given question.

Answer:
The following code
public final class Algorithm {
    public static <T> T max(T x, T y) {
        return x > y ? x : y;
    }
} will " Not Compile" because ">" operator is only used for " Primitive Type only".