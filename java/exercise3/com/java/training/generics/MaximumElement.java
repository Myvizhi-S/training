package com.java.training.generics;

/* 
Requirement:
    To find the maximum element of the given program using generic method.

Entity:
    public class MaximalElement

Function Declaration:
    public static void main(String[] args)
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    
Jobs to be Done:
    1) Import the util package.
    2) Compare the elements and find the maximum between the range and return it
    3) Under the main method create an object list kor List<Integer>
    4) Call the method max() and print the result.
*/

import java.util.*;

public class MaximumElement {
    
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
        T maximumElement = list.get(begin);
        for (++begin; begin < end; ++begin) {
            if (maximumElement.compareTo(list.get(begin)) < 0) {
                maximumElement = list.get(begin);
            }
        }   
        return maximumElement;
    }
    
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(max(list, 6, 0));
    }

}
