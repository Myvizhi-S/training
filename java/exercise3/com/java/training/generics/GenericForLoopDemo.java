package com.java.training.generics;

/* 
 Requirement:
     To write a program to demonstrate generics - for loop, for list, set and map.

 Entity:
     GenericForLoopDemo
     
 Function Declaration:
     public static void main(String[] args)
     
 Jobs to be Done:
     1) Import Set, HashSet, ArrayList and HashMap.
     2) Demonstrate generics using Set and HashSet and print it
     3) Demonstrate generics using ArrayList and print it.
     4) Demonstrate generics using HashMap and print it.

 */

import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;

public class GenericForLoopDemo {
    
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Myvizhi");
        set.add("Sharanya");
        set.add("Volley Ball");
        System.out.println("The set is " + set);
        for (String string : set) {
            System.out.println("The elements are " + string);
        }
        
        ArrayList<Double> list = new ArrayList<>();
        list.add(12.99);
        list.add(15.9876);
        list.add(17.509875);
        System.out.println("The list is " + list);
        for (double numbers : list) {
            System.out.println("the elements of the list are " + numbers);
        }
        
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Myvizhi");
        hashMap.put(2, "Sharanya");
        hashMap.put(3, "Kiruthiga");
        System.out.println("The hashMap is " + hashMap);
        for (Integer key : hashMap.keySet()) {
            String value = hashMap.get(key);
            System.out.println(key + ":" + value);
        }
        
        for (String value : hashMap.values()) {
            System.out.println("The values are " + value);
        }
    }
}
