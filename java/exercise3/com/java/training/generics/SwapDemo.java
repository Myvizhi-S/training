package com.java.training.generics;

/*
Requirement:
    To write a generic Method to swap the elements.

Entity:
    public class SwapDemo

Function Declaration:
    public static <T> void swap(T[] list, int firstNumber, int lastNumber)
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber)
    public static void main(String[] args)

Jobs to be done:
    1) Import packages Arrays, Collections, List, ArrayList.
    2) Create a method swap() of type T and swap the elements using temperary variables.
    3) Create another method printSwap() and swap by using Collections.swap().
    4) Under a main method create an array and call the swap method.
    5) print the result.
    6) Create an object for List of type Integer and call printSwap() method
    7) Print the result.
*/

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class SwapDemo {
    
    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
        T temperaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temperaryVariable;
        System.out.println("The swapped element is " + Arrays.toString(list) );
    }
    
    @SuppressWarnings("unused")
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
        Collections.<T> swap(list1, firstNumber, secondNumber);
    }
    
    public static void main(String[] args) {
        Integer[] list = {10, 20, 30, 40, 50, 60, 100};
        swap(list, 3, 6);
        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, 3, 6);
        System.out.println(list1);
        
    }
}
