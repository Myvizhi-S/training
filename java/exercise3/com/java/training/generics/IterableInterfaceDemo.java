package com.java.training.generics;

/*
Requirement:
    To  Write a program to print employees name list by implementing iterable interface.

Entity:
    class Employee.
    class EmployeeList.
    public class IterableInterfaceDemo.

Function Declaration:
    public String getFirstName()
    public void setFirstName(String firstName)
    public String getlastName()
    public void setlastName(String lastName)
    public int getEmployeeId() 
    public void setEmployeeId(int employeeId)
    public String toString() 
    public void addEmployee(Employee employee)
    public void removeEmployee(Employee employee)
    public static void main(String[] args)
    public int employeesListSize()
    public Iterator<Employee> iterator()
    
Jobs to be done:
    1) Import lang and util packages.
    2)Declare the variables firstName, lasttName of typeString and employeeId of type
       integer.
    3) Create a constructor and read the values.
    4) Using getters and setters get and set emloyeeName, employeeMobileNumber and employeeId.
    5) Return the result by converting to String by using toString() method.
    6) Now create a class EmployeeList of type Employee by implementing from Iterable.
    7) Create a constructor and store the values as list.
   10) Now add and remove the employee using addEmployee() method removeEmployee method().
   11) Create a class IterableInterfaceDemo as public.
   12) Under a main method Create an object for Employee as employee1 and employee2.
   13) Create an object for EmployeeList and add the values from employee1 and employee2.
   14) Using for each loop print the list. 
*/

import static java.lang.String.format;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

class Employee {

    private String employeeName;
    private String employeeMobileNumber;
    private int employeeId;

    public Employee(String employeeName, String employeeMobileNumber, int employeeId) {
        this.employeeName = employeeName;
        this.employeeMobileNumber = employeeMobileNumber;
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setFirstName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeMobileNumber() {
        return employeeMobileNumber;
    }

    public void setEmployeeMobileNumber(String employeeMobileNumber) {
        this.employeeMobileNumber = employeeMobileNumber;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
    
    @Override
    public String toString() {
        return format("employeeName: %s employeeMobileNumber: %s EmployeeId: %d", employeeName, employeeMobileNumber, employeeId);
    }
}

class EmployeeList implements Iterable<Employee> {
    private List<Employee> employees;
    
    public EmployeeList() {
        employees = new ArrayList<Employee>();
    }
    
    public EmployeeList(int employeeId) {
        employees = new ArrayList<Employee>(employeeId);
    }
    
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }
    
    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }
    
    public int employeesListSize() {
        return employees.size();
    }

    @Override
    public Iterator<Employee> iterator() {
        // TODO Auto-generated method stub
        return employees.iterator();
    }
}

public class IterableInterfaceDemo {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Myvizhi S", "6379036665", 12345);
        Employee employee2 = new Employee("Sharanya K K", "6389075643", 12346);
        EmployeeList employeeList = new EmployeeList();
        employeeList.addEmployee(employee1);
        employeeList.addEmployee(employee2);
        for (Employee employee : employeeList) {
            System.out.println(employee);
        }
    }
}