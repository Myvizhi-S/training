Requirement:
    To find whether the following snippet is valid or not and to fix it.
    (int x, y) -> x + y;

Entity:
    No entity is declared.

Function Declaration:
    No function is declared.

Jobs to be done:
    To look the following snippet and to answer the question.

Answer:
We cannot have the lambda expression for declaration of type of only one of the parameter is
explicity mentioned. So it is "invalid".

The correct snippet is given below:
(int x, int y) -> x + y;