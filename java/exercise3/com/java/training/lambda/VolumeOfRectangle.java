package com.java.training.lambda;

/* 
Requirement:
    To print the volume of the rectangle using Lambda Expressions.
    
Entity:
    public class VolumeOfrectangle.

Function Declaration:
    public double printVolume(double length, double width, double height)
    public static void main(String[] args)

Jobs to be done:
    1) Create a interface Rectangle.
    2) Under a main method Create a lambda expression and call the method and print the result.     
*/

interface Rectangle {
    
    public double printVolume(double length, double width, double height);
}

public class VolumeOfRectangle {
    
    public static void main(String[] args) {
        Rectangle volume = (length, width, height) -> {
            double volumeOfRectangle = length * width * height;
            return (volumeOfRectangle);
        };
        
        System.out.println("The volume of the rectangle is: " + volume.printVolume(2.50, 3.00, 1.00));
    }
}
