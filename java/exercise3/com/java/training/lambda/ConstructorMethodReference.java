package com.java.training.lambda;

/*
Requirement:
    To explain about the MethodInterface and its types.

Entity:
    public class ConstructorMethodReference
    class Message

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.lambda
    2. Declare the class ConstructorMethodReference as public.
    3. create a class Message.
    4. Create an interface MessageDemo
    5. It is an example for constructor method Reference
    5. Under a main method Create a lambda expression and call the method and print the result.     
*/

interface MessageDemo {

    Message getMessage(String message);
}

class Message {
    
    public Message(String message) {
        System.out.println(message);
    }
}

public class ConstructorMethodReference {
    
    public static void main(String[] args) {
        MessageDemo string = Message :: new;
        string.getMessage("Hello World");
    }
}
