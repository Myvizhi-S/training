package com.java.training.lambda;

/*
Requirement:
    To code the simple lambda expression using the method getValue()

Entity:
    public class SimpleLambdaDemo 

Function Declaration:
    public int getValue()
    public static void main(String[] args)
    
Jobs to be done:
    1) Create a interface LambdaDemo.
    2) Under a main method Create a lambda expression and call the method and print the result.     
*/

interface LambdaDemo {
    
    public int getValue();
}

public class SimpleLambdaDemo {
    
    public static void main(String[] args) {
        LambdaDemo value = () -> 555;
        System.out.println("The value is: " + value.getValue());
    }
}
