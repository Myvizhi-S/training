package com.java.training.lambda;

/*
Requirement:
    To print the sum of the variable arguments.

Entity:
    public class LambdaVariableArguments

Function declaration:
    public int printAddition(int ... numbers)
    public static void main(String[] args)
    
Jobs to be done:
    1) Create a interface VariableArgumentDemo.
    2) Under a main method Create a lambda expression and call the method and print the result.  
 */
interface VariableArgumentDemo {
    
    public int printAddition(int ... numbers);
}

public class LambdaVariableArguments {
    
    public static void main(String[] args) {
        VariableArgumentDemo value = (numbers) -> {
            int sum = 0;
            
            for (int values : numbers) {
                sum = sum + values;
            }
            
            return sum;
        };
        
        System.out.println("The sum of elements is: " + value.printAddition(9, 25, 78, 53));
    }
}
