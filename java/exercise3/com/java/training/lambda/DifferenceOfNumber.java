package com.java.training.lambda;

/*
Requirement:
    To write a program to print difference of two numbers using lambda expression and the single method interface

Entity:
    public class DifferenceOfNumber

Function Declaration:
    public int printDifference(int firstNumber, int secondNumber)
    public static void main(String[] args)

Jobs to be done:
    1) Create a interface NumberDifference.
    2) Under a main method Create a lambda expression and call the method and print the result.     
*/

interface NumberDifference {
    
    public int printDifference(int firstNumber, int secondNumber);
}

public class DifferenceOfNumber {
    
    public static void main(String[] args) {
        NumberDifference difference = (firstNumber, secondNumber) -> (secondNumber - firstNumber);
        System.out.println(difference.printDifference(200, 500));
    }

}
