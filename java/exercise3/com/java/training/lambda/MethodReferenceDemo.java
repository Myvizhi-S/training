package com.java.training.lambda;


/*
Requirement:
    To explain about the MethodInterface and its types.

Entity:
    public class MethodReferenceDemo

Function Declaration:
    public static void print(final int number)
    public static void main(String[] args)

Jobs to be done:
    1) Import the List and array package.
    2) Create an object for list of type Integer and initialize it and convert it to Array.
    3) It is an example for static method demo
    4) Under a main method Create a lambda expression and call the method and print the result.     
    
 */
/*
They are effectively a subset of lambda expressions, because if a lambda expression can be used,
then it might be possible to use a method reference, but not always. They can only be used to call 
singular method, which obviously reduces the possible places they can be used, unless your code is
written to cater for them. It would be a good idea if you knew the notation for a method reference.

Four Types:
    1. Reference to a static method
    2. Reference to an instance method of a particular object
    3. Reference to an instance method of an arbitrary object of a particular type
    4. Reference to a constructor
*/

import java.util.List;
import java.util.Arrays;

public class MethodReferenceDemo {
    
    // Reference to a static method
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        // Method reference
        list.forEach(MethodReferenceDemo::print);
        // Lambda expression
        list.forEach(number -> MethodReferenceDemo.print(number));
        // normal
        for(int number : list) {
            MethodReferenceDemo.print(number);
        }
    }
    
    public static void print(final int number) {
        System.out.println("I am printing: " + number);
    }
}    
