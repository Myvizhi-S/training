package com.java.training.lambda;

/*
Requirement:
    To write a Lambda expression program with a single method interface to concatenate two strings.

Entity:
    public class ConCatDemo.

Function Declaration:
    public String conCat(String firstString, String secondString)
    public static void main(String[] args)

Jobs to be done:
    1) Create a interface ConcatinationDemo.
    2) Under a main method Create a lambda expression and call the method and print the result. 
*/

interface ConcatinationDemo {
    
    public String conCat(String firstString, String secondString);
}

public class ConcatDemo {
    
    public static void main(String[] args) {
        ConcatinationDemo conCatString = (firstString, secondString) -> (firstString + secondString);
        System.out.println(conCatString.conCat("Siva", "Sharanya"));
    }
}