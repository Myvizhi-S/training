package com.java.training.lambda;

/*
Requirement:
  To convert the given Anonymous class to Lambda expression.
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}

Entity:
    public class ConvertToLambdaDemo

Function Declaration:
    public boolean isEven(int value)
    public static void main(String[] args)

Jobs to be done:
    1) Create a interface CheckNumber.
    2) Under a main method Create a lambda expression and call the method and print the result.     
*/

interface CheckNumber {
    
    public boolean isEven(int value);
}

public class ConvertToLambdaDemo {
    
    public static void main(String[] args) {
        CheckNumber evenOrOdd = (value) -> {
            
            if (value % 2 == 0) {
                return true;
            } else {
                return false;
            }
        };
        
        System.out.println("is the number even?  " + evenOrOdd.isEven(798));
    }

}
