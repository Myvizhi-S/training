package com.java.training.lambda;

/*
Requirement:
    To achieve Addition,Substraction,Multiplication and Division concepts using Lambda expression
    and functional interface

Entity:
    LambdaExpresssionDemo
    Arithmetic

Function declartion:
    int operation(int a, int b)

Jobs to be done:
    1) Create interface under the main method declare the method operation.
    2) Under the main method define the methods which is declared in interface as Addition,Subtracrion,Multiplication and Divison.
    3) Print the results as per the function declared.
 */

interface Arithmetic {
	int operation(int a, int b);
}


public class LambdaExpresssionDemo {
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b);
		System.out.println("Addition = " + addition.operation(5, 5));

		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(8, 3));

		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(4, 5));

		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(9, 3));

	}
}
