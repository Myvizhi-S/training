package com.java.training.map;


import java.util.HashMap;

public class CopyMapDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap1 = new HashMap<>();
        hashMap1.put(1, "Red");
        hashMap1.put(2, "Green");
        hashMap1.put(3, "Black");
        System.out.println("The values of hashMap1 is" + " " + hashMap1);
        
        HashMap<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put(4, "White");
        hashMap2.put(6, "Blue");
        hashMap2.put(5, "Orange");
        System.out.println("the value of hashMap2 is" + " " + hashMap2);
        
        hashMap2.putAll(hashMap1);
        System.out.println("Now the value of hashMap2 is" + " " + hashMap2);
    }

}