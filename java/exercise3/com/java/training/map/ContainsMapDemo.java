package com.java.training.map;

/*
Requirement:
    To write a Java program to test if a map contains a mapping for the specified key.

Entity:
    public class ContainsMapDemo.
    
Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1) Import the util package HAshMap.
    2) Create an object for a HashMap hashMap and Store the values for hashMap.
    3) Now create a variable called value of type boolean and check the keys is present in the hashMap
    4) Print the result stored in the value.   
*/

import java.util.HashMap;

public class ContainsMapDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(10, "Actor");
        hashMap.put(25, "Vijay");
        hashMap.put(20, "Vijay");
        hashMap.put(76, "Vijay");
        hashMap.put(12, "Vijay");
        boolean value = hashMap.containsKey(11);
        System.out.println("if the key 11 presents in hashMap" + " " + value);
        
        boolean value1 = hashMap.containsKey(20);
        System.out.println("if the key 20 presents in hashMap" + " " + value1);
    }

}
