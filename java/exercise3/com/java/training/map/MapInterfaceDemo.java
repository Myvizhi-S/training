package com.java.training.map;

/* Requirement:
      To demonstrate java program to working of map interface using put(), remove(), 
      booleanContainsValue(), replace() 
  Entity:
       MapInterfaceDemo
  Function declaration:
       public static void main(String[] args) 
  Jobs to be done:
       1)Create a treemap
       2)add elements to the treemap.
       3)print the treemap.
       4)Remove the key value pair.
       5)print the treemap.
       6)check value is present and print the result as boolean
       7)Replace particular value
       8)print the treemap
       
  Pseudo code:
		public class MapInterfaceDemo {

			public static void main(String[] args) {
				//create a new treemap
				//add elements to the treemap
				treemap.remove();
				System.out.println(treemap);
				System.out.println(treemap.containsValue());
				System.out.println(treemap);
				treemap.replace();
				System.out.println(treemap);		
			}
		}
*/

import java.util.TreeMap;

public class MapInterfaceDemo {

	public static void main(String[] args) {

		TreeMap<String, String> treeMap = new TreeMap<>();
		treeMap.put("Name" , "Myvizhi.S");
		treeMap.put("Class" , "III-EEE");
		treeMap.put("Roll No" , "18ee046");
		treeMap.put("Subject" , "Maths");
		treeMap.put("Mark" , "100");
		System.out.println("treemap:" + treeMap);
		treeMap.remove("Class");
		System.out.println("After removing:" + treeMap);
		System.out.println(treeMap.containsValue("Roll No"));
		treeMap.replace("Name", "Sharanya.K.K");
		System.out.println("After replacing the value of Name:" + treeMap);
	}

}
