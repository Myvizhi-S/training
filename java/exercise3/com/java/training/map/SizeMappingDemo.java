package com.java.training.map;

import java.util.HashMap;

/*
Requirement:
    To write the java program to count size of mappings.

Entity:
    public class SizemappingDemo

Function Declaration:
    public static void main(String[] args)
    size()

Jobs to be done:
    1. Create a package com.java.training.map.
    2. Import the util package.
    3. Create a class SizeMappingDemo.
    4. Now create a object for HashMap as hashMap and store the String values for
       integer keys.
    5. Now print the count of size of mappings.
*/

public class SizeMappingDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Bharathwaj");
        hashMap.put(2, "Ajay");
        hashMap.put(3, "Aniruth");
        hashMap.put(4, "Vijay");
        hashMap.put(5, "Sethupathy");
        hashMap.put(6, "Lawrance");
        hashMap.put(8, "Sugan");
        hashMap.put(7, "Ilan");
        System.out.println("the keys and values are " + hashMap);
        System.out.println("the count of the sizre of the mappings are " + hashMap.size());
    }

}