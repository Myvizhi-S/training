package com.java.training.collection;

/*
Requirement:
    To use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() 
    methods to store and retrieve elements in ArrayDequeue.
    
Entity:
    ArrayDequeDemo
    
Method Signature:
    public static void main(String[] args)

Jobs to be Done:
    1. Create an object for ArrayDequeue as dequeue and add the elements to it.
    2. Now use using the methods given in the question store and retrieve the dequeue. 
*/



import java.util.ArrayDeque;

public class ArrayDequeDemo {
    
    public static void main(String[] args) {
        ArrayDeque<String> deque = new ArrayDeque<>();
        deque.add("Myvizhi");
        deque.add("Sharanya");
        deque.add("Kiruthiga");
        deque.add("Water");
        deque.add("Cool");
        deque.add("Hot");
        deque.add("Ring");
        deque.add("Teddy");
        System.out.println("The ArrayDeque " + deque);
        
        deque.addFirst("Mother");
        System.out.println("After adding the first element" + deque);
        
        deque.addLast("Brother");
        System.out.println("After adding the last element" + deque);
        
        deque.removeFirst();
        System.out.println("After removing the first element" + deque);
        
        deque.removeLast();
        System.out.println("After removing the last element" + deque);
        
        System.out.println("The first peek element " + deque.peekFirst());
        System.out.println("The last  peek element " + deque.peekLast());
        
        System.out.println("The first poll element " + deque.pollFirst());
        System.out.println("The last poll element " + deque.pollLast());

    }

}