package com.java.training.collection;

/*
Requirement:
    To convert 8 districts Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy
    to UPPERCASE.
Entity:
    Districts
Function Declaration:
    public static void main(String[] args)
Jobs to be done:
    1) Create reference for list and assign all values given.
    2) Change all the string value in upper case.
    3) Print the list. 
 */


import java.util.Arrays;
import java.util.List;

public class Districts {

	public static void main(String[] args) {
		List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur",
				"Salem", "Erode", "Trichy");
		list.replaceAll(String::toUpperCase);
		System.out.println(list);
	}

}