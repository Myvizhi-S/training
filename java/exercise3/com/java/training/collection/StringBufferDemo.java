package com.java.training.collection;
/*
Requirements:
    To demonstrate insertions and string buffer in tree set. 
     
Entities:
    public class StringBufferDemo 
      
Function Declaration:
    public static void main(String[] args)
      
Jobs To Done:
    1) Create the object tree set type as StringBuffer as Generic type.
    2) Add the string values.
    3) Prints the output.
*/

import java.util.TreeSet;

public class StringBufferDemo {

	public static void main(String[] args) {
		TreeSet<StringBuffer> treeSet = new TreeSet<>();
		treeSet.add(new StringBuffer("A"));
		treeSet.add(new StringBuffer("B"));
		treeSet.add(new StringBuffer("C"));
		treeSet.add(new StringBuffer("D"));
		treeSet.add(new StringBuffer("E"));
		System.out.println(treeSet);
	}

}

/*
Explanation:
String class and all the Wrapper classes already implements Comparable interface but StringBuffer 
class doesn�t implements Comparable interface. Hence, we get a ClassCastException in the above 
example.
*/