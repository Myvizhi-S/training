package com.java.training.collection;

/*
Requirement:
    To demonstrate the difference between poll() method and remove() method.

Entity:
    RemoveAndPoll

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1. Create an object for Queue and add the elements to the Queue.
    2. By using pop() method print the result.
    3. By using the remove() Method print the result and demonstrate the difference between pop() and
       remove()
*/
import java.util.Queue;
import java.util.PriorityQueue;
public class RemoveAndPoll {
    
    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue<>();
        queue.add(555);
        queue.add(556);
        queue.add(557);
        queue.add(558);
        queue.add(559);
        queue.poll();
        System.out.println("After poll " + queue);
        
        queue.clear();
        System.out.println("After clear " + queue.poll());  // prints null.
        
        Queue<String> string = new PriorityQueue<>();
        string.add("Jackson");
        string.add("Ema Watson");
        string.add("He was assasinated");
        string.add("Come and get your Material");
        string.add("Thiis is my program");
        string.add("Mahesh");
        string.remove();
        System.out.println("After remove method " + string);
        
        string.clear();
        System.out.println("After the clear method " + string.remove()); // throws exception
    }
}

/* 
From this example the poll() is used to remove or retrieve the top element and when the queue is empty
it returns the null but remove() throws exception.
*/
