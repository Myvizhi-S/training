package com.java.training.network;

/*
Requirement:
    To create a program for httpUrlConnection.
    
Entity:
    HttpUrlConnectionDemo
    
Method signature:
    public static void main(String[] args) 
    
Jobs to be done:
    1. Create an instance for URL that stores the url for javatpoint.
    2. huc which is an instance of HttpUrlConnection that stores an object of UrlConnection class.
    3. print the response code for the http url(huc)
    
Pseudo code:
class HttpUrlConnectionDemo {
    
    public static void main(String[] args) throws Exception {   
            URL urlName = new URL("http://www.javatpoint.com/java-tutorial");    
            HttpURLConnection huc=(HttpURLConnection)urlName.openConnection();  
            System.out.println(huc.getResponseCode());
    }
}
 */

import java.net.HttpURLConnection;
import java.net.URL;

public class UrlConnectionDemo {
    
    public static void main(String[] args) throws Exception {   
            URL urlName = new URL("http://www.javatpoint.com/java-tutorial");    
            HttpURLConnection huc=(HttpURLConnection)urlName.openConnection();  
            System.out.println(huc.getResponseCode());
    }
}
