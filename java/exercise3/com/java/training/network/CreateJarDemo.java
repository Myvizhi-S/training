package com.java.training.network;

/*
Requirement:
    int nextToken()

Entity:
    CreateJar

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1) Create a path instance for a file which has to be created.
    2) By using createFile method ,file will be created.
 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateJarDemo {

    public static void main(String[] args) throws IOException {
        Path sourceFile = Paths.get(
                "C:\\Users\\Myvizhi\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\network\\create.jar");
        Files.createFile(sourceFile);
        System.out.println("Jar file created successfully ");
    }
}
