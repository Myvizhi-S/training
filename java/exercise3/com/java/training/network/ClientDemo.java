package com.java.training.network;

/*
Requirement:
    Build a client side program using ServerSocket class for Networking

Entity:
    Client

Method Signature:
    public static void main(String[] args)
Jobs to be done:
    1) Print client requesting server.
    2) ServeSocket channel is created by invoking the open method of this class.
    3) Creates a socket address with a specified port number.
    4) Print the clients's Remoteaddress.
    5) print client is ready to receive.
    6) Close the socket channel.

Pseudo code:
class Client {

    public static void main(String[] args) throws IOException {
        System.out.println("Client requesting Server");

        ServerSocketChannel serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(99));
        SocketChannel client = serverSocket.accept();

        System.out.println(IP address);
        System.out.println("Client is ready to receive");
        client.close();
    }
}

 */  

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class ClientDemo {

    public static void main(String[] args) throws IOException {

        System.out.println("Client requesting Server");

        ServerSocketChannel serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(99));
        SocketChannel client = serverSocket.accept();

        System.out.println("Connection Set: " + client.getRemoteAddress());
        System.out.println("Client is ready to receive");
        client.close();
    }
}
