package com.java.training.properties;


/*
 Requirement:
 
    Write a program to add 5 elements to a xml file and print the elements in the xml file using list. Remove the 3rd element from the xml file and print the xml file.
Entities:
    PropertyXML

Function Signature:
    public static void main(String[] args)

Jobs to be done:
    1) Create a object for properties.
    2) Setting some property as (key - value) pair in properties.
    3) Create a object for FileOutputStream and assign a name for xml file.
    4) Store the properties in FileOutputStream as xml file.
    5) Print the properties stored in xml file successfully
    6) Print the elements in xml file using list. 
    7) Remove the 3rd element and print the properties.
 
Pseudo code:
class PropertyXML {
 
    public static void main(String[] args) throws IOException {
        //Add the element to the properties.
        OutputStream output = new FileOutputStream("Filename.xml");
        //store the property element to the Outputstream.
        System.out.println("Properties stored in xml file successfully");
        
        properties.list(System.out);

        properties.remove("3rd elemeent key);
        System.out.println("After removing the element from the xml file ");
        properties.list(System.out);
        }
  }   
        
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class PropertyXML {

    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.setProperty("India", "New Delhi");
        properties.setProperty("Pakistan", "Islamabad");
        properties.setProperty("Sri Lanka", "Colombo");
        properties.setProperty("Nepal", "Kathmandu");

        OutputStream output = new FileOutputStream("Capitals.xml");
        properties.storeToXML(output, "Countries and Capitals.");
        System.out.println("Properties stored in xml file successfully");

        properties.list(System.out);

        properties.remove("Sri Lanka");
        System.out.println("After removing the element from the xml file ");
        properties.list(System.out);
        
    }

}
