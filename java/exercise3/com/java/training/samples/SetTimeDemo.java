package com.java.training.samples;

public class SetTimeDemo {
    
    public static void main(String[] args) {
      long time = System.currentTimeMillis();
      // create a date
      java.sql.Date date = new java.sql.Date(time);
      date.setTime(1999924);
      System.out.println("New date: " + date.toString());
   }
}
