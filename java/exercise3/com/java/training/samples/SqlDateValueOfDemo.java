package com.java.training.samples;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;

public class SqlDateValueOfDemo {
    
    public static void main(String[] args) {
        Date sqlDate1 = Date.valueOf("2020-09-26");
        System.out.println("SqlDate1: " + sqlDate1);
        
        Date sqlDate2 = Date.valueOf(LocalDate.of(2020, Month.SEPTEMBER, 26));
        System.out.println("SqlDate2: " + sqlDate2);
            
    }
}
