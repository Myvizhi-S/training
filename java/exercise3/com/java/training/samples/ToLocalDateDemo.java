package com.java.training.samples;

import java.sql.Date;

public class ToLocalDateDemo {
    public static void main(String [] args) {  
        
        long currentTime=System.currentTimeMillis();  
        Date date=new Date(currentTime);  
        System.out.println("Current LocalDate is = "+date.toLocalDate());  
    }
}