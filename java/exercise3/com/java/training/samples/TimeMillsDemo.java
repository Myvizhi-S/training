package com.java.training.samples;

public class TimeMillsDemo {
    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        // returns Current time in milliseconds
        System.out.println("The current time is " + time);
    }

}
