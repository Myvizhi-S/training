package com.java.training.samples;

import java.sql.Date; 

public class StringToSqlDateDemo { 

    public static void main(String[] args) {
        String str = "2020-03-07";  
        Date date = Date.valueOf(str); //converting string into sql date  
        System.out.println(date);
    }
}
