package com.java.training.samples;

import java.sql.Date;

public class SqlDateDemo {
    
    public static void main(String[] args) {
        long now = System.currentTimeMillis();
        Date sqlDate = new Date(now);
        System.out.println("currentTimeMillis: " + now);
        System.out.println("SqlDate          : " + sqlDate);
        System.out.println("SqlDate.getTime(): " + sqlDate.getTime());
    }
}
