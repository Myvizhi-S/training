package com.java.training.samples;

public class TimeDifferenceDemo {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        System.out.println(start);
        for(int i=0;i<100;i++) {
            System.out.println("difference between start and end time");
        }
        long end = System.currentTimeMillis(); 
        System.out.println(end);
        long difference = end - start;
        System.out.println("The Difference between start and end time is " + difference);
    }

}
