package com.java.training.stream;

/*
 
Requirement:
    To sort the roster list based on the person's age in descending order using comparator
 
Entity:
    SortComparator
 
Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an object for list as roster and assign a createRoster method to it.
    2. By comparing the age of a person in a person's list,
        2.1) put it in the stream in descending order and print it
*/

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class SortComparator {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        roster.sort((Comparator.comparing(Person::getAge)));
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.dateOfBirth + " "
                + person.gender + " " + person.emailId));
    }
}