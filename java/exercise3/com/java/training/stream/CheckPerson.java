package com.java.training.stream;

/*
Requirement:
    Consider the following Person:
        new Person(
           "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
    - Check if the above person is in the roster list obtained from Person class.
  
Entity:
	CheckPerson
  
Function Declaration:
	public static void main(String[] args)
  
Jobs To Be Done:
    1. Declare an boolean variable present and assign it to false.
    2. 
 */

import java.time.chrono.IsoChronology;
import java.util.List;
import java.util.stream.Stream;

public class CheckPerson {

	public static boolean present = false;

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
				Person.Sex.MALE, "bob@example.com");
		Stream<Person> stream = roster.stream();
		stream.forEach(person -> {
			if ((person.getName().equals(newPerson.getName()))
					&& (person.getBirthday().equals(newPerson.getBirthday()))
					&& (person.getEmailId().equals(newPerson.getEmailId()))
					&& (person.getGender().equals(newPerson.getGender()))) {
				present = true;
			}
		});
		if (present == true) {
			System.out.println("Person is present");
		} else {
			System.out.println("Person is not present");
		}

	}
}
