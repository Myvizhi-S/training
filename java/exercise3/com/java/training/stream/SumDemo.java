package com.java.training.stream;

/*
Requirement:
  Consider a following code snippet:
       List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
       - Find the sum of all the numbers in the list using java.util.Stream API
       - Find the maximum of all the numbers in the list using java.util.Stream API
       - Find the minimum of all the numbers in the list using java.util.Stream API 
Entities:
  SumDemo
  
Function Declaration:
  public static void main(String[] args) 
     
Jobs To Be Done:
    1. Create list of Integers whose reference is randomNumbers and convert it to array
    2. Using Stream API answer the given question.
*/

import java.util.Arrays;
import java.util.List;

public class SumDemo {
	
    public static void main(String[] args) {
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
		System.out.println("List elements are " + randomNumbers);
		
		int number1 = randomNumbers.stream().mapToInt(Integer::valueOf).sum();
		System.out.println("Sum of the list elements " + number1);
		
		int number2 = randomNumbers.stream().max(Integer::compare).get();
		System.out.println("Maxium element in the list " + number2);
		
		int number3 = randomNumbers.stream().min(Integer::compare).get();
		System.out.println("Minimum element in the list " + number3);
	}

}
