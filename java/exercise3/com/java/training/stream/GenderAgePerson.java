package com.java.training.stream;

/*
Requirement:
    To Write a program to filter the Person, who are male and age greater than 21
  
Entity:
  	GenderAgePerson
  
Function Declaration:
   	public static void main(String[] args)
  
Jobs To Be Done:
    1. Access the precreated  roaster list referred from Person.java.
    2. Iterate every element in the Person and check for,
        2.1) person's gender is male and
        2.2) person's age is greater than 21
        2.3) Print those person's name. 

*/

import java.util.List;

public class GenderAgePerson  {
    
    public static void main(String[] args) {
    	List<Person> roster = Person.createRoster();  
    
    	for (Person p : roster) {
    	    if (p.getGender() == Person.Sex.MALE ) {
    	    	if(p.getAge() > 21) {
    	        System.out.print(p.getName() + " ");
    	    	}
    	    }
   
	    }
    }
}