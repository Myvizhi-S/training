package com.java.training.stream;

/*
Requirement:
    Iterate the roster list in Persons class and and print the person without using forLoop/Stream   
  
Entity:
    Iterate

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. a reference roster for list is created and createoster method is assigned to it.
    2. By using Iterator iterate a person list
    3. First an object is created for roster and based on the condition iterate a roster.
          3.1) upto the roster has the next element the process is continued 
          3.2) Print the roster.
       
*/

import java.util.Iterator;
import java.util.List;

public class Iterate {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		Iterator<Person> iterator = roster.iterator();
		
		while (iterator.hasNext()) {
			Person person = iterator.next();
			System.out.println(person.name + " " +person.dateOfBirth + " " +person.gender + " " + person.emailId);
		}
	}
}
