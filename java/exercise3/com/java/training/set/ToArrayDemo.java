package com.java.training.set;

/*Requirement:
    To demonstrate linked hash set to array() method in java
  
  Entity:
    ToArrayDemo
    
  Function Declaration:
    public static void mian(String[] args){ }
    
  Jobs To Be Done:
     1)Create a linkedhashset
     2)Add elements to the linkedhashset
     3)create a string array
     4)Convert the linkedhashset to array and store in array
     5)iterte the array
        5.1)print the element in the array
  
   Pseudo code:
     public class ToArrayDemo {
        
        public static void main(String[] args) {
           
            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
            add elements to the linkedhashset
            String[] array = new String[];
            array = linkedHashSet.toarray(array);
            for(int number = 0 ;number < arr.length ;number++) {
			       System.out.println(array[number]);
	      	}
        }       
    }
 */

import java.util.LinkedHashSet;

public class ToArrayDemo {
	
public static void main(String[] args) {
		
		LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
		
		linkedHashSet.add("fish");
		linkedHashSet.add("turtle");
		linkedHashSet.add("sponge");
		linkedHashSet.add("dolphin");
		
		System.out.println(linkedHashSet);
		
		Object[] arr = linkedHashSet.toArray();
		
		for(int number = 0 ;number < arr.length ;number++) {
			System.out.println(arr[number] );
		}
	}
}
