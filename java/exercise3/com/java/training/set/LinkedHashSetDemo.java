package com.java.training.set;
/*
Requirement:
    To demonstrate program explaining basic add and traversal operation of linked hash set

Entity:
    LinkedHashSetDemo
Function Declaration:
    public static void main(String[] args){} 
Jobs To Be Done:
    1)Create a LinkedHashSet
    2)Add elements to the linkedHashSet
    3)create a iterator
    4)iterate the hashmap
        4.1)print the element in the linkedhashset
    
Pseudocode:
    public class LinkedHashSetDemo {
        
        public static void main(String[] args) {
           
            LinkedHashSet<Double> linkedHashSet = new LinkedHashSet<>();
            add elements to the linkedhashset
            Iterator<Double> iterator = new Iterstor<Double>();
            while(iterator.hasNext()){
            
                 System.out.Pritln(print the element) ;
            }
        }
    }
*/

import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

	public static void main(String[] args) {

		LinkedHashSet<Double> linkedHashSet = new LinkedHashSet<>();

		linkedHashSet.add(6.66);
		linkedHashSet.add(7.88);
		linkedHashSet.add(7.77);
		linkedHashSet.add(8.99);
		
		System.out.println(linkedHashSet);

		Iterator<Double> iterator = linkedHashSet.iterator();

		while(iterator.hasNext()) {
			
			System.out.println(iterator.next());
		}
	}

}