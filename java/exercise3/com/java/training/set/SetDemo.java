package com.java.training.set;

/*Requirement:
    Java program to demonstrate adding elements, displaying, removing, and iterating in hash set
 
  Entity:
     SetDemo
  
  Function Declaration:
     public static void main(String[] args)
  
  Jobs To Be Done:
     1)Create a HashSet .
     2)Add the elements to the hashset.
     3)Print the hashset
     4)Remove the element in the hashset.
     5)Iterate the hashset.
     
  Pseudo code:
    public class SEtDemo {
      
      public static void main(String[] args) {
      		
     	   HashSet<Integer> hashset = new HashSet<Integer> ;
           
           for(number = 0 ;number <= 10 ;number++) {
                 add element to the hashset
           }
           System.out.println("hashset":hashset);
           Remove an element in the hashset
          
           for(number=0 ; number <= 5 ; number++) {
                   hashset.add(number);
           }
      }
  }
 */


import java.util.HashSet;
import java.util.Iterator;

public class SetDemo {
	
    public static void main(String[] args) {
		HashSet<Integer> hashSet = new HashSet<Integer>() ;
	    for(int number = 0 ;number <= 10 ;number++) {
		    hashSet.add(number) ;
		          
	    }        

        //display
		System.out.println("hashset:" + hashSet);
		          
		//remove
		hashSet.remove(10);
		System.out.println("After Removing:" + hashSet);
		          
		//Iterating
		Iterator<Integer> iterator = hashSet.iterator();
		while(iterator.hasNext()) {
		    Integer element = iterator.next();
		    System.out.println("After Iterating: " + element);
		}	
	}

}
