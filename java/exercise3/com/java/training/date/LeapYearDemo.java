package com.java.training.date;

/*
Requirement:
    1. To check whether the current or given year is leap or not.
    2. To find the length of the year.
    
Entity:
    LeapYearDemo.

Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Get the current year and store it in the variable currentYear.
    2. Print that currentYear.
    3. Check whether the currentYear is leap year,
           3.1) If it is a leap year, print "the current year is leap year"
           3.2) If it is not a leap year, print "the current year is not a leap year".
    4. Find the length of the currentYear and store it in lengthOfCurrentYear.
           4.1) print the length of the current year.
    5. Get the user given year and store it in givenYear and print givenYear.
    6. Check whether the giventYear is leap year,
           6.1) If it is a leap year, print "the given year is leap year"
           6.2) If it is not a leap year, print "the given year is not a leap year".
    6. Find the length of the givenYear and store it in lengthOfGivenYear.
           4.1) print the length of the given year.
    
Pseudo code:
class LeapYearDemo {

   public static void main(String[] args) {
        Year currentYear = Year.now();
        System.out.println("The current year is: " + currentYear);

        if (currentYear.isLeap() == true) {
            System.out.println("The current year is a leap year");
        } else {
            System.out.println("The current year is not a leap year");
        }
        
        int lengthOfCurrentYear = currentYear.length();
        System.out.println("The length of the current year is: " + " " + lengthOfCurrentYear);
        
        Year givenYear = Year.of(2001);
        System.out.println("The given year is: " + " " + givenYear);
        
        if (givenYear.isLeap() == true) {
            System.out.println("The current year is a leap year");
        } else {
            System.out.println("The current year is not a leap year");
        }
        
        int lengthOfGivenYear = currentYear.length();
        System.out.println("The length of the current year is: " + " " + lengthOfGivenYear);
    }

}
*/

import java.time.Year;

public class LeapYearDemo {
    
    public static void main(String[] args) {
        Year currentYear = Year.now();
        System.out.println("The current year is: " + currentYear);

        if (currentYear.isLeap() == true) {
            System.out.println("The current year is a leap year");
        } else {
            System.out.println("The current year is not a leap year");
        }
        
        int lengthOfCurrentYear = currentYear.length();
        System.out.println("The length of the current year is: " + " " + lengthOfCurrentYear);
        
        Year givenYear = Year.of(2001);
        System.out.println("The given year is: " + " " + givenYear);
        
        if (givenYear.isLeap() == true) {
            System.out.println("The current year is a leap year");
        } else {
            System.out.println("The current year is not a leap year");
        }
        
        int lengthOfGivenYear = currentYear.length();
        System.out.println("The length of the current year is: " + " " + lengthOfGivenYear);
    }

}
