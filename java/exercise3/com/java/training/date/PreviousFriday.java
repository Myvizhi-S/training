package com.java.training.date;

/*
Requirement:
    To find the date of the previous Friday for a given random date.

Entity:
    PreviousFriday

Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1.Get the input date From the user
    2.Invoke the LocalDate class and pass the parameter 
      which you get from user
    3.Using LocalDate class's object get the Previous Friday 's date and Print it .
    
Pseudo Code:

public class PreviousFriday {
    
    public static void main(String[] args) {
        Scanner scanner// use scanner and get year,month,day
        LocalDate date = LocalDate.of(year,month,day);
        System.out.printf("Previous friday is:  %s,%n", date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}
*/

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class PreviousFriday {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the year month and date");
		int year = scanner.nextInt();
		int month = scanner.nextInt();
		int date = scanner.nextInt();
		LocalDate thisdate = LocalDate.of(year,month,date);
		System.out.printf("The previous Friday is: %s%n",thisdate.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
		scanner.close();
	}
}
