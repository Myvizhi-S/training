package com.java.training.date;

/*
Requirement:
    To find maximum and minimum value of the year, Month, week and day of a default calendar.
    
Entity:
    MaximumAndMinimumDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create an instance of a Calendar as calendar that gets the calendar using current time zone
    2. Now create a integer variable maximumYear to store the actual maximum year of the default calendar.
    3. Print the maximumYear.
    4. Now create a integer variable maximumMonth to store the actual maximum month of the default calendar.
    5. Print the maximumMonth.
    6. Now create a integer variable maximumWeek to store the actual maximum week of the default calendar.
    7. Print the maximumWeek.
    8. Now create a integer variable maximumDay to store the actual maximum day of the default calendar.
    9. Print the maximumDay.
    10. Now create a integer variable minimumYear to store the actual minimum year of the default calendar.
    11. Print the minimumYear.
    12. Now create a integer variable minimumMonth to store the actual minimum month of the default calendar.
    13. Print the minimumMonth.
    14. Now create a integer variable minimumWeek to store the actual minimum week of the default calendar.
    15. Print the minimumWeek.
    16. Now create a integer variable minimumDay to store the actual minimum day of the default calendar.
    17. Print the minimumDay.
    
 Pseudo code:
 class MaximumAndMinimumDemo {
     
     public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        int maximumYear = calendar.getActualMaximum(Calendar.YEAR);
        System.out.println("The maximum year of defaukt calendar is: " + " " + maximumYear);
        
        int maximumMonth = calendar.getActualMaximum(Calendar.MONTH);
        System.out.println("The maximum month of default calendar is: " + " " + maximumMonth);
        
        int maximumWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        System.out.println("The maximum week of default calendar is: " + " " + maximumWeek);
        
        int maximumDay = calendar.getActualMaximum(Calendar.DATE);
        System.out.println("The maximum day of default calendar is: " + " " + maximumDay);
        
        int minimumYear = calendar.getActualMinimum(Calendar.YEAR);
        System.out.println("The minimum year of the default calendar is: " + " " + minimumYear);
        
        int minimumMonth = calendar.getActualMinimum(Calendar.MONTH);
        System.out.println("The minimum month of the default calendar is: " + " " + minimumMonth);
        
        int minimumWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
        System.out.println("The minimum week of the default calendar is: " + " " + minimumWeek);
        
        int minimumDay = calendar.getActualMinimum(Calendar.DATE);
        System.out.println("The minimum day of the default calendar is: " + " " + minimumDay);
    }
}
*/

import java.util.Calendar;

public class MaximumAndMinimumDemo {
    
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        int maximumYear = calendar.getActualMaximum(Calendar.YEAR);
        System.out.println("The maximum year of defaukt calendar is: " + " " + maximumYear);
        
        int maximumMonth = calendar.getActualMaximum(Calendar.MONTH);
        System.out.println("The maximum month of default calendar is: " + " " + maximumMonth);
        
        int maximumWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        System.out.println("The maximum week of default calendar is: " + " " + maximumWeek);
        
        int maximumDay = calendar.getActualMaximum(Calendar.DATE);
        System.out.println("The maximum day of default calendar is: " + " " + maximumDay);
        
        int minimumYear = calendar.getActualMinimum(Calendar.YEAR);
        System.out.println("The minimum year of the default calendar is: " + " " + minimumYear);
        
        int minimumMonth = calendar.getActualMinimum(Calendar.MONTH);
        System.out.println("The minimum month of the default calendar is: " + " " + minimumMonth);
        
        int minimumWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
        System.out.println("The minimum week of the default calendar is: " + " " + minimumWeek);
        
        int minimumDay = calendar.getActualMinimum(Calendar.DATE);
        System.out.println("The minimum day of the default calendar is: " + " " + minimumDay);
    }
}