package com.java.training.date;

/*
Requirement:
    To say which would use to store birthdays in year, months, days, seconds, and nanoseconds.
    
Answer:
    1. Mostly LocalDateTime class is used to store birthdays in year, months, days, seconds, and
       nanoseconds.
    2. In case of a particular time zone, ZoneDateTime class is used.
    3. Both the classes tracks the date and time to nano second.
    4. When both the classes used in conjunction with period, gives a result using a combination 
       of human-based units, such as years, months, and days. 
    */