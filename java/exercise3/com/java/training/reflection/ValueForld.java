package com.java.training.reflection;

/*
Requirement:
	To write a code to set values for id and print it.
  
Entity:
	ValueForId
  
Method Signature:
	public static void main(String[] args) {}
  
Jobs to be Done:
	1)Assign the already defines user file to the concreteClass reference placed in the package "com.kpr.training.reflections.User".
	2)Pass the value to the User and assign the value to the id.
	3)Print the value for the field id.
  
Pseudo Code:
class ValueForId {
	public static void main(String[] args) {
		Class concreteClass = Class.forName("com.kpr.training.reflections.User");
		Field field = concreteClass.getField("id");
  
		System.out.println(field.getInt(user));             
  				
  			}
  		}
 */

import java.lang.reflect.Field;

public class ValueForld {
	
    public static void main(String[] args) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
    	Class<?> concreteClass = null;
		try {
			concreteClass = Class.forName("com.java.training.User");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	User user = new User(10);
    	Field field = concreteClass.getField("id");
    	System.out.println(field.getInt(user));

    	// Set value to concreteClass2
    	field.set(user, 20);

    	// Get value from concreteClass2 object
    	System.out.println(field.getInt(user));
    }
}

