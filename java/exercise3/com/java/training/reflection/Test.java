package com.java.training.reflection;

/*
Requirements:
   complete:
   i)Deprecate the display method
   ii)how to suppress the deprecate message 
class DeprecatedTestTest 
{   
    //method to be deprecated
    public void Display() 
    { 
        System.out.println("Deprecatedtest display()"); 
    } 
} 
  
public class Test 
{ 
 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 

Entity:
  	  DeprecatedTest
     Test
  
Methods Signature:
     public void Display() 
     public static void main(String[] args) 
 
Jobs To Be Done:
     1.Add Deprecated annotation for the given program and print the result.
     2.Remove the Deprecated annotation, to suppress the deprecated message for the given program and print the result. 
  

Pseudo code:
class DeprecatedTest {
	@Deprecated
	public void Display() {
		System.out.println("Deprecatedtest display() ");
	}
}


public class Test {

	public static void main(String[] args) {
		DeprecatedTest d1 = new DeprecatedTest();
		d1.Display();
	}
}
*/

class DeprecatedTest {
	public void Display() {
		System.out.println("Deprecatedtest display() ");
	}
}


public class Test {

	public static void main(String[] args) {
		DeprecatedTest d1 = new DeprecatedTest();
		d1.Display();
	}
}
