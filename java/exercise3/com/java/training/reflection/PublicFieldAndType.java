package com.java.training.reflection;

/*
Requirement:
  	 To write a program to obtain the all public fields and field type for the given code User.java.
  
Entity:
  	PublicFieldAndType
  
Method Signature:
  	public static void main(String[] args)
  
Jobs To Be Done:
  	1)Assign a predefined class to the reference variable.
  	2)Get the field names of that class and assign it to the variables.
  	3)For each fields,
  		3.1)print the field name and its type.
  
Pseudo Code:
class PublicFieldAndType  {
  
	public static void main(String[] args) {
		Class clas = java.lang.Thread.class;
		Field[] fields= clas.getFields();
		for(int i = 0; i< fields.length; i++) {
			System.out.println(fields[i] + fields[i].getType());
  		}
  				
    } 
}
*/

import java.lang.reflect.*;

public class PublicFieldAndType {
	public static void main(String[] argv) throws Exception {
		Class<?> clas = java.lang.Thread.class;
		Field[] fields = clas.getFields();
		
		for (int i = 0; i < fields.length; i++) {
			System.out.println("The Field is: " + fields[i].toString() + " and type is: "
					+ fields[i].getType());

		}
	}
}
