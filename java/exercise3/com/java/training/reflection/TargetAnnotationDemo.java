package com.java.training.reflection;

/*
Requirements:
  create a Program to demonstrate @Target annotation (@Target) using interface annotation (@interface)
 
Entity:
    TargetAnnotationDemo
  
Method Signature:
    public static void main(String[] args)
    @interface AnnotationDemo{}
    static @TypeAnnoDemo String method() 
  
Jobs To Be Done:
     1)Declare the Target annotation with Element Type.
     2)Declare the interface.
     3)Create a string variable with type String and invoke the interface method and print it.
     4)Invoke the interface method with type string and print it.

Pseudo Code:
@Target(ElementType.TYPE_USE)

@interface AnnotationDemo {
}


public class TargetAnnotationDemo {

	public static void main(String[] args) {
		@AnnotationDemo
		String string = "I am annotated with a type annotation";
		System.out.println(string);
		method();
	}

	static @AnnotationDemo String method() {
		System.out.println("This function's  return type is annotated");
		return null;
	}
}

*/

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE_USE)

@interface AnnotationDemo {
}


public class TargetAnnotationDemo {

	public static void main(String[] args) {
		@AnnotationDemo
		String string = "I am annotated with a type annotation";
		System.out.println(string);
		method();
	}

	static @AnnotationDemo String method() {
		System.out.println("This function's  return type is annotated");
		return null;
	}
}
