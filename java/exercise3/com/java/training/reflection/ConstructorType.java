package com.java.training.reflection;

/*
Requirement:
    To explain the types of constructors with an example.
    
Entity:
    ConstructorType
    
Function declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Declare a integer and boolean variable.
    2. Create a constructor method without any arguements.
        2.1 Print a message.
    3. Create a constructor method with arguements.
        3.1 Print a message.
    4. Create objects for the class.
    5. Print the value and result as default constructor.
    
Pseudo code:
class ConstructorType {
    int value;
    boolean result;

    public ConstructorType() {
        System.out.println(message);
    }

    public ConstructorType(String type) {
        System.out.println(message);
    }

    public static void main(String[] args) {

        ConstructorType constructorType1 = new ConstructorType();
        ConstructorType constructorType2 = new ConstructorType("Parameterized");
        System.out.println(constructorType1.value);
        System.out.println(constructorType1.result);
    }
}

*/

public class ConstructorType {
    
    int value;
    boolean result;

    public ConstructorType() {
        System.out.println("This is no arguement constructor");
    }

    public ConstructorType(String type) {
        System.out.println("This is " + type + "constructor");
    }

    public static void main(String[] args) {

        ConstructorType constructorType1 = new ConstructorType();
        @SuppressWarnings("unused")
        ConstructorType constructorType2 = new ConstructorType("Parameterized");
        System.out.println("Example of default constructor");
        System.out.println(constructorType1.value);
        System.out.println(constructorType1.result);
    }
}
