package com.java.training.reflection;

/*
Requirements :
    Invoke a public and private method .
   
Entity;
   InvokeMethods
   Check
   
Function Signature:
   public static void main(String[] args)
   
Jobs to be Done:
   1)Create a object for the class check.
   2)Call a Method in that class .
       2.1)Print the Statement .
   
   
Pseudo code:
class Check {

    private void private_Method() {
        System.out.println("The Private Method  called from outside");
    }

    public void printData() {
        System.out.println("The Public Method called from outside ");
    }
  }

  class InvokeMethods {

    public static void main(String[] args) throws Exception {
        Check check = new Check();

        // call the private method .

        method.setAccessible(true);
        method.invoke(check);
    }
  }
    
 */

import java.lang.reflect.Method;

class Check {

    @SuppressWarnings("unused")
    private void privateMethod() {
        System.out.println("The Private Method called from outside");
    }

    public void printData() {
        System.out.println("The Public Method called from outside ");
    }
}

public class InvokeMethods {

    public static void main(String[] args) throws Exception {
        Check check = new Check();
        Method method = Check.class.getDeclaredMethod("privateMethod"); 
        method.setAccessible(true);
        method.invoke(check);
    }
}

  