package com.java.training.reflection;

/*
Requirements:
    Write a Java program to demonstrate the use of reflection.
   
Entity:
    Methods
   
Function Signature:
    1)Create an object for the stack library .
    2)Get the methods in stack and store it on methods .
    3)For each method.
         3.1)Print the method
  
Pseudo code :
class Methods {
    public static void main(String[] args) {
        try {
            Class c = Class.forName("java.util.Stack");
            //Get the methods in stack
            for (int count = 0; count < methods.length; count++) {
                System.out.println(methods[count].toString());
            }
        } catch (Throwable e) {
            System.err.println(e);
        }
    }
}
   
 */

import java.lang.reflect.*;

public class Methods {
    public static void main(String[] args) {
        try {
            Class<?> name = Class.forName("java.util.Stack");
            Method[] methods = name.getDeclaredMethods();
            
            for (int count = 0; count < methods.length; count++) {
                System.out.println(methods[count].toString());
            }
        } catch (Throwable e) {
            System.err.println(e);
        }
    }
}
