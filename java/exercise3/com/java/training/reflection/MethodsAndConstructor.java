package com.java.training.reflection;

/*
Requirement :
   Differentiate methods and constructors
   
Explanation :
    1) Constructor is used to initialize an object whereas method is used to exhibits functionality of an object.
    2) Constructors are invoked implicitly whereas methods are invoked explicitly.
    3) Constructor does not return any value where the method may/may not return a value.
    4) In case constructor is not present, a default constructor is provided by java compiler.
       In the case of a method, no default method is provided.
    5) Constructor should be of the same name as that of class. Method name should not be of the same name as that of class.
 */

