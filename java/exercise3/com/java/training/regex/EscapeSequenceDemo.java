package com.java.training.regex;


/*
Requirement:
    To write a program to display the following using escape sequence in java
        A. My favorite book is "Twilight" by Stephanie Meyer
        B. She walks in beauty, like the night, Of cloudless climes and starry skies 
            And all that's best of dark and bright Meet in her aspect and her eyes�
        C. "Escaping characters", � 2019 Java

Entity:
    EscapeSequenceDemo

Function Declaration:
    public static void main(String[] args) 
    
Jobs to be done:
    Use the proper escape sequence to print the given results.
    
Pseudo code:
class EscapeSequenceDemo {

    public static void main(String[] args) {
        System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
        System.out.println("She walks in beauty, like the night, \r\n"
                           + "Of cloudless climes and starry skies \r\n"
                           + "And all that's best of dark and bright \r\n"
                           + "Meet in her aspect and her eyes....") ;
        System.out.println("\"Escaping characters\", � 2019 Java");
    }

}
*/
public class EscapeSequenceDemo {

    public static void main(String[] args) {
        System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
        System.out.println("She walks in beauty, like the night, \r\n"
                           + "Of cloudless climes and starry skies \r\n"
                           + "And all that's best of dark and bright \r\n"
                           + "Meet in her aspect and her eyes....") ;
        System.out.println("\"Escaping characters\", � 2019 Java");
    }

}
