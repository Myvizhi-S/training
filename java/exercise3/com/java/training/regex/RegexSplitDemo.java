package com.java.training.regex;


/*
Requirement:
    To use the split() method for the code String website = "https-www-google-com";
    
Entity:
    RegexSplitDemo.
    
Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a String variable website and assign the value to it.
    2. Also create a variable of type String denoting where to begin split function.
    3. Create a pattern that compiles the that String variable created.
    4. Now use the split method  and store it in the array of String
    5. Fetch every element in the String array and print it.
    
Pseudo code:
class RegexDemo {
    
    public static void main(String[] args) {
        String website = "https-www-google-com";
        String splitPattern = "-";
        Pattern pattern = Pattern.compile(splitPattern);
        
        String[] split = pattern.split(text);
        // using for loop fetch the values.
    }
}

*/

import java.util.regex.Pattern;
import java.util.regex.Matcher;

@SuppressWarnings("unused")
public class RegexSplitDemo {
    
    public static void main(String[] args) {
        String text = "https-www-google-com";
        String splitPattern = "-";
        Pattern pattern = Pattern.compile(splitPattern);
        
        String[] split = pattern.split(text);
        
        for(String element : split) {
            System.out.println(element);
        }
    }

}
