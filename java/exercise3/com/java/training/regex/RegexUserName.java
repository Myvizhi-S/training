package com.java.training.regex;


/*
Requirement:
    create a username for login website which contains
    8-12 characters in length
    Must have at least one uppercase letter
    Must have at least one lower case letter
    Must have at least one digit

Entity:
    RegexUserName
 
Function Declaration:
    public static boolean validUserName(String UserName);
    public static void main(String[] args)
     
Jobs to Done:
    1) Create the object for Scanner.
    1) Get the username from the user .
    2) Create the pattern for the Username.
    3) Check whether the input matches the pattern
           3.1) if it matches return true.
           3.2) if it not matches return false.
    4)Check if returned value is true 
          4.1) print entered username is valid.
          4.2) print entered username is invalid.
 
Pseudo code:
      
class RegexUserName {

    public static boolean validUserName(String userName) {
        String regex = "^(?=.[0-9])" + "(?=.[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,12}$";
        //Pattern pattern or String pattern
        if (input != pattern)
            return false;
        else
            return true;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userName = scanner.next();
        System.out.println(validUserName(userName));
    }
}

*/

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUserName {

	public static boolean validUserName(String userName) {
		String regex = "^(?=.[0-9])" + "(?=.[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,12}$";
        Pattern pattern = Pattern.compile(regex);
		
        if (userName == null) {
			return false;
		}
		
		Matcher matcher = pattern.matcher(userName);
		return matcher.matches();
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the UserName ");
		String userName = scanner.next();
		if (validUserName(userName))
			System.out.print("Username is valid");
		else
			System.out.print("Username is invalid");
		scanner.close();
	}
}
