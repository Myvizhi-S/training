package com.java.training.regex;


/*
Requirement:
    To code the java string regex methods.

Entity:
    RegexDemo

Function declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Get the user String input as text.
    2. Check whether the given user input text is null or not.
        2.1) If it is null then print as "give the valid String"
        2.2) If it is not null then,
             2.2.1) declare the another String as regex whose pattern is to be matched.
             2.2.2) Now compile that pattern and match it.
        2.3) If the match found print the beginning and end of that string.
             2.3.1) Continue this process until the end of the String text.
             
Pseudo code:
class RegexDemo {
    
    public static void main(String[] args) {
        String text = Get a Scanner input
        if text == null
            // print " Give a Valid String";
        else
            String regex = "is";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);
            int count = 0;
            while (matcher.find()) {
                count++;
                // print starting and end of the regex given
             }
    }
}

*/

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class RegexDemo {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        
        if (text == null) {
            System.out.println("Give the valid text and try again!!!");
        } else {
            String regex = "is";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);
            
            int count = 0;
            while (matcher.find()) {
                count++;
                System.out.println("found: " + count + ": " + matcher.start() + " - " + matcher.end());
            }
        }
        
        scanner.close();
    }
}
