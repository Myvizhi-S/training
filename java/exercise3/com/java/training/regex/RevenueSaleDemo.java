package com.java.training.regex;


/*
Requirement:
    To write a Java program to calculate the revenue from a sale based on the unit price 
    and quantity of a product input by the user.
    
Entity:
    RevenueSaleDemo
    
Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Declare a floating point variable unitPrice  and floating variable revenue and assign revenue to 0.0.
    2. Declare a integer variable quantity and a floating variables of discountPrice and discountAmount.
           2.1) Assign the value as 0f for the variables discountPrice and discountAmount.
    3. Get the user input for quantity and unitPrice and check,
           3.1) if the quantity is less than 100 units then,
                    3.1.1) the value of revenue is given by the product of quantity and unitPrice.
           3.2) if the quantity is between 110 and 120 then,
                    3.2.1) calculate the discountPrice(10%).
                    3.2.2) now product the quantity and unitPrice the result is multiplied with discountPrice.
                               3.2.2.1) the result is stored in discountAmount.
                    3.2.3) Now the value of revenue is calculated by subtracting the product of unitPrice and quantity
                           with the discountAmount.
            3.3) if the quantity is greater than 120 then,
                    3.3.1) Calculate the discountPrice which is 15%.
                    3.3.2) Calculate product of quantity and unitPrice and store it in revenue.
                    3.3.3) calculate the product of discountPrice and revenue and store it in discountAmount.
                    3.3.4) Obtain the value of revenue by subtracting the last revenue value with discountAmount.
    4. Print the revenue and discountAmount values.
    
Pseudo code:
class RevenueSaleDemo {
    
    public static void main(String[] args) {
        float revenue = 0.0;
        float discountPrice = 0f;
        float discountAmount = 0f;
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the unitPrice: ")
        float unitPrice = scanner.nextFloat();
        System.out.println("Enter the quantity: ");
        int quantity = scanner.nextInt();
        
         if (quantity < 100) {
             revenue = unitPrice * quantity;
         } else if (quantity >= 110 && quantity <= 120) {
             discountPrice = 10 / 100;
             revenue = unitPrice * quantity;
             discountAmount = revenue * discountPrice;
             revenue -= discountAmount;
         } else if (quantity > 120) {  
             discountPrice = 15 / 100;
             revenue = unitPrice * quantity;
             discountAmount = revenue * discountPrice;
             revenue -= discountAmount;
         }
         
         System.out.println(revenue);
         System.out.println(discountAmount);
    }
}
*/
import java.util.Scanner;

public class RevenueSaleDemo {
    
    public static void main(String[] args) {
        float discountPrice;
        float discountAmount = 0f;
        float revenue = 0f;
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the unitPrice: ");
        float unitPrice = scanner.nextFloat();
        
        System.out.print("Enter the quantity: ");
        int quantity = scanner.nextInt();
        
        if (quantity < 100) {
            revenue = unitPrice * quantity;
        } else if (quantity >= 110 && quantity <= 120) {
            discountPrice = (float) 10 / 100;
            revenue = unitPrice * quantity;
            discountAmount = revenue * discountPrice;
            revenue -= discountAmount;
        } else if (quantity > 120) {
            discountPrice = (float) 15 / 100;
            revenue = unitPrice * quantity;
            discountAmount = revenue * discountPrice;
            revenue -= discountAmount;
        }
        
        System.out.println("The revenue of sale is: " + revenue + "$");
        System.out.println("After discount sale: " + discountAmount + "$");
        scanner.close();
        
    }

}
