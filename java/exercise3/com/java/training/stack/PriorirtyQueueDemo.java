package com.java.training.stack;

/*
Requirement:
    To complete the following code and to find its output.
     Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());   

Entity:
    PriorityQueueDemo.

Function Declaration:
    public static void main(String[] args)
    poll(), peak().

Jobs to be Done:
    1) Import util package.
    2) Create an object for Queue as bike and add the elements to it.
    3) Use poll method to retrieve and delete the top element.
    4) Now print the peak element of the queue.
*/

import java.util.*;

class PriorityQueueDemo {
    
    public static void main(String[] args) {
        Queue<String> bike = new PriorityQueue<>();
        bike.add("Shine");
        bike.add("CBR 120");
        bike.add("FZ");
        bike.add("R15");
        bike.poll();
        System.out.println(bike.peek());
    }

}
