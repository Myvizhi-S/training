package com.java.training.stack;

/*
Requirement:
    Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
    1. add atleast 5 elements
    2. remove the front element
    3. search a element in stack using contains key word and print boolean value value
    4. print the size of stack
    5. print the elements using Stream 

Entity
    public class QueueDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Import the Queue, LinkedList, PriorityQueue and Stream packages.
    2) Create an object string for the Queue class of String type and add elements and 
       print it.
    3) Now print the removed element and create a value of type boolean and print the result.
    4) Print the size of the queue and print the elements using Stream.
    5) Use the same procedure for the PriorityQueue and print the results.
 */

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueDemo {
    
    public static void main(String[] args) {
        Queue<String> string = new LinkedList<>();
        string.add("Bike");
        string.add("Car");
        string.add("Airlines");
        string.add("Raillines");
        string.offer("Lorry");
        System.out.println("The list elements are" + string);
        System.out.println("The removerd element is" + string.remove());
        System.out.println("the queue after removal of first element is" + string);
        // searching using contains keyword
        boolean value = string.contains("Track");
        System.out.println("the boolean value is" + value);
        // To print the size of the stack
        System.out.println("the size of the queue is" + string.size());
        
        Stream<String> stream = string.stream();
        System.out.println("The queue elements printed using Stream ");
        stream.forEach(iteration -> System.out.print(iteration + " "));
       
        Queue<Integer> integer = new PriorityQueue<>();
        integer.add(10);
        integer.add(20);
        integer.add(30);
        System.out.println("The list elements are" + integer);
        System.out.println("The removerd element is" + integer.remove());
        System.out.println("the queue after removal of first element is" + integer);
        // searching using contains keyword
        boolean value1 = string.contains("Track");
        System.out.println("the boolean value is" + value1);
        // To print the size of tghe stack
        System.out.println("the size of the queue is" + integer.size());
        
        Stream<Integer> stream1 = integer.stream();
        System.out.print("The queue elements printed using Stream ");
        stream1.forEach(iteration1 -> System.out.print( iteration1 + " "));
        
    }
    

}
