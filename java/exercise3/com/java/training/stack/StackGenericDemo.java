package com.java.training.stack;

/*
Requirement:
    1. To create a stack using generic type and implement.
    2. Push at least 5 elements.
    3. Pop the peek element.
    4. search a element in stack and print index value.
    5. print the size of stack.
    6. print the elements using Stream.
    
Entity:
    StackGenericDemo

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1) Import Stack and Stream package.
    2) Create an object for a Stack as integer.
    3) Now pushing the six elements of the stack as 10, 20, 30, 40, 50, 60.
    4) Now printing the peek popped element, printing the index of searched value in stack
       and printing the elements using stream.
 
 */
import java.util.Stack;
import java.util.stream.Stream;

public class StackGenericDemo {
    
    public static void main(String[] args) {
        Stack<Integer> integer = new Stack<>();
        integer.push(10);
        integer.push(20);
        integer.push(30);
        integer.push(40);
        integer.push(50);
        integer.push(60);
        System.out.println("The peek element is" + " " + integer.peek());
        System.out.println("the popped element is" + " " + integer.pop());
        System.out.println("The Stack after pop is " + integer);
        System.out.println("The index of the element 30 is" + integer.indexOf(30));
        System.out.println("the size of the stack is " + integer.size());
        
        Stream<Integer> stream = integer.stream();
        System.out.print("The Stack elements printed using Stream ");
        stream.forEach(i -> System.out.print( i + " "));
    }

}
