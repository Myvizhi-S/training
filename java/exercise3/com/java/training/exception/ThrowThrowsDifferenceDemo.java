package com.java.training.exception;

/*
Requirement:
    To demonstrate the difference between throw and throws.

Entity:
    public class ThrowThrowsDiffernceDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Create a method chackAge() to demonstrate the example for throw.
    2) Create a method division() to demonstrate throws clause.
    3) Print the results. 
*/

public class ThrowThrowsDifferenceDemo {

    public void checkAge(int age) {
        
        // Throw keyword is used to throw an exception explicitly.
        if (age < 18) {
            throw new ArithmeticException("Not Eligible for voting");
        } else {
            System.out.println("Eligible for voting");
        }
    }

    // Throws is used to declare an exception, which means it works similar to the try-catch.
    public int division(int a, int b) throws ArithmeticException {
        int t = a / b;
        return t;
    }

    public static void main(String args[]) {
        ThrowThrowsDifferenceDemo object = new ThrowThrowsDifferenceDemo();
        object.checkAge(13);
        System.out.println("End Of Program");
        
        ThrowThrowsDifferenceDemo object1 = new ThrowThrowsDifferenceDemo();
        try {
           System.out.println(object1.division(15,0));  
        }
        catch (ArithmeticException e) {
           System.out.println("You shouldn't divide number by zero");
        }
    }
}
