package com.java.training.exception;

/*
Requirement:
    To say whether we can use only try without catch and finally.

Entity:
    public class TryWithoutCatchAndFinallyDemo

Function Declaration:
    public static void main(String[] args)
    public void printFile()

Jobs to be done:
    1) Import io package.
    2) Create a class TryWithoutCatchAndFinallyDemo.
    3) Use try with resource without the use of catch and finally.
    4) We can use only try block by using try with resource.
*/

import java.io.*;

public class TryWithoutCatchAndFinallyDemo {
    
    private void printFile() throws IOException {
        
        // try with resource
        // We can use only try without using catch and Finally with try with resource.
        // It produces Auto-Closable package eventhough exception is handled.
        try(FileInputStream input = new FileInputStream("file.txt")) {

            int data = input.read();
            while(data != -1){
                System.out.print((char) data);
                data = input.read();
            }
        }
    }
    
    public static void main(String[] args) throws IOException {
        TryWithoutCatchAndFinallyDemo demo = new TryWithoutCatchAndFinallyDemo();
        demo.printFile();
    }
   
}
