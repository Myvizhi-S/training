package com.java.training.exception;

/*
Requirement:
    To demonstrate the importance of Validation.

Entity:
    public class ValidationDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Import Scanner from util package.
    2) The importance of validation is to check the user input to the given format.
    3) It is also used to handle the input mismatch exception.
    4) Under a main method declare an integer variable number, String variable string
       and boolean variable to be initialized to false.
    5) By using while loop iterate until the the correct value is to be entered by using try
       and catch block.
    6) Print the result.
*/

import java.util.Scanner;

public class ValidationDemo {
    
    public static void main(String[] args) {
    	int number = 0;
        String string;
        boolean valid = false;
        
        // setup Scanner.
        Scanner scanner = new Scanner(System.in);
        // keep looping until valid input
        while (valid == false) {
            System.out.print("Enter an integer value ");
            // Grab input from user for string
            string = scanner.nextLine();
            // try to convert string to integer
            try {
                number = Integer.parseInt(string);
                valid = true;
            } catch (NumberFormatException e) {
                System.out.println("oops!! Enter an integer value");
            }
        }
        
        System.out.println("the number you entered is " + number);
        scanner.close();
    }
}