package com.java.training.exception;

/*
Requirement:
    To demonstrate the difference between Checked and Unchecked Exception.

Entity:
    public class CheckedAndUncheckedDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Import the package FileInputStream.
    2) Now compare checked with unchecked.
    3) The exception thrown at the compile time is called Checked Exception.
    4) The exception thrown at the run time is called unChecked Exception.
*/

//import java.io.FileInputStream;

public class CheckedAndUncheckedDemo {

    public static void main(String[] args) {
        // Example for an unchecked Exception.
        // Exception does not be produced during run time.
        int[] array = {1, 2, 3, 4, 5, 6};
        System.out.println("The 7th element is " + "  " + array[6]);
        
        // Checked Exception - Throws compile time exception.
        //It throws FileNotFoundException if the file is not found
        /* FileInputStream file = null;
        file = new FileInputStream("C:/myfile.txt");
        int text;
        while ((text = file.read()) != -1) {
            System.out.println((char)text);
        }
        
        file.close(); // Throws IO exception.*/
    }

}
