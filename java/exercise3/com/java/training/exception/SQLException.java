/*
Requirement:
    To explain the below program.
        try{
            dao.readPerson();
        } catch (SQLException sqlException) {
            throw new MyException("wrong", sqlException);
      }

Entity:
    No entity is used.

Function Declaration:
    There is no function declared.

Jobs to Be done:
    1. To expalin the following.

The answer is:
    Here the "Exception Wrapping" is used. TheThe method dao.readPerson() can throw an SQLException.
    If it does, the SQLException is caught and wrapped in a MyException. Notice how the SQLException
    (the sqlException variable) is passed to the MyException's constructor as the last parameter.
