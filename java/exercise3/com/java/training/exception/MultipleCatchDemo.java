package com.java.training.exception;

/*
Requirement:
    To Demonstrate catching multiple exception.

Entity:
    public class MultipleCatchDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Under a main method declare a try block.
    2) The try block contains the initialization of an integer array named array of size 6.
    3) Now declare the value of the index as 36 / 0.
    4) Then print the array.
    5) Now give multiple exception block stating Arithmetic exception ArrayIndexOutOfBoundsException.
    6) Finally give the print statement "rest of code"  
*/

public class MultipleCatchDemo {
    
    public static void main(String[] args) {
        try {    
            int[] array=new int[6];    
            array[6]=36/0;    
            System.out.println(array[12]);  
           } catch (ArithmeticException e) { 
               System.out.println("Arithmetic Exception occurs");  
           } catch(ArrayIndexOutOfBoundsException e) { 
               System.out.println("ArrayIndexOutOfBounds Exception occurs");  
           } catch(Exception e) {
               System.out.println("Parent Exception occurs");  
           }             
           
        System.out.println("rest of the code");    
    }
}