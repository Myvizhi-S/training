package com.java.training.exception;

/*
Requirement:
    To write a program using try and catch.

Entity:
    public class ListOfNumber.

Function Declaration:
    public static void main(String[] args)
    public voidwriteList()

Jobs to be done:
    1) Create an integer array arrayOfNumbers of size 10.
    2) Declare the method public void writeList()
    3) The method body contains try and catch block of NumberFormatException and
       IndexOutOfBounds exception.
    4) Under a main method create an object for the class ListOfNumber as list.
    5) Now call the writeList() method and print the result.
*/

public class ListOfNumber {
    
    public int[] arrayOfNumbers = new int[10];
    
    public void writeList() {
        try {
            arrayOfNumbers[10] = 11;
        } catch (NumberFormatException e1) {
            System.out.println("NumberFormatException:" + " " + e1.getMessage());
        } catch (IndexOutOfBoundsException e2) {
            System.out.println("IndexOutOfBoundsException:" + " " + e2.getMessage());
        }
    }
        
    public static void main(String[] args) {
        ListOfNumber list = new ListOfNumber();
        list.writeList();
    }
}