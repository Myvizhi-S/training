package com.java.training.exception;

/*
Requirement:
    To handle the given program.
       public class Exception {  
           public static void main(String[] args) {
               int arr[] ={1,2,3,4,5};
               System.out.println(arr[7]);
           }
       }
Entity:
    public class HandleDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Under the main method try block is created and an integer array is created of 5 elements.
    2) Now print the 8th element in an integer array.
    3) Now give an catch block of ArrayIndexOutOfBoyundsException and common Exception and print
       the results.
*/

public class HandleDemo {
    
    public static void main(String[] args) {
        try {
            int[] array = {1,2,3,4,5,6};
            System.out.println(array[7]);
        } catch (ArrayIndexOutOfBoundsException e1) {
            System.out.println("Not in the index level");
        } catch (Exception  e2) {
            System.out.println("The exception is produced");
        }
    }
}
            