package com.java.training.exception;

/*
Requirement:
    To demonstrate whether a multiple try block can be used foe single catch.

Entity:
    public class MultipleTryBlockDemo 

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Under a main method declare the two variable number1 and number2 as type int.
    2) Declare the try block getting command line arguments of number1 and number2.
    3) Now declare the another try block declare the variable number3 of type integer
    4) number3 stores the division of number1 and number2.
    5) Give the catch block of exception.
    6) print the statement.
*/
public class MultipleTryBlockDemo {
    
    public static void main(String[] args) {
        @SuppressWarnings("unused")
        int number1;
        @SuppressWarnings("unused")
        int number2;
       // Error occurs try needs catch or finally to complete
       // But we can use nested try block it is valid
       /* try {
            number1 = Integer.parseInt(args[0]);
            number2 = Integer.parseInt(args[1]);
        
            try {
                int number3 = number1 / number2;
                System.out.println("number3");
            } catch (Exception e) {
                System.out.println("Give the arguments");
            }
        } */
    }
}