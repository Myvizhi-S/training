package com.java.training.streamtokanizer;

/*
  Requirement:
  		To Write java programs for stream tokenizer methods :-
			1) commentChar() method
			2) eollsSignificant() method
			3) lineno() method
			4) lowerCaseMode() method
	
	Entity:
		StreamTokanizerMethodDemo
		
	Method Signature:
		public static void main(String[] args)
		
	Jobs To be Done:
		1) Create a file and write some contents to it.
		2) Read that file and access it using StreamTokeizer 
		3) In commentChar method it prints the character before the character mentioned to it.
		4) In eollSignificant method , it checks the end of the line 
		5) lineno method return the line no of current position.
		6) lowerCaseMode method that converts the file data into lower case.
	
	Psseudo Code:
		class StreamTokenizerMethodDemo {
			public static void main(String[] args) {
				File myObj = new File("ABC.txt");
				FileWriter myWriter = new FileWriter("ABC.txt");
			    myWriter.write("Hello Everyone" + "\n" + "1 \n 2\n 3" + "By Myvizhi S" + "Here is the example program for Stream tokenizer methods" );
			    myWriter.close();
			    
			    //Read that file using StreamTokenizer 
			     
			    token.eolIsSignificant(true);
			    token.commentChar('a');
			    token.lowerCaseMode(arg1);
			    System.out.println("Line Number:" + token.lineno());
			    
			    int t;
				while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) {
					switch (t) {
						case StreamTokenizer.TT_EOL: 
	                		System.out.println("End of Line encountered."); 
	                		break; 
						case StreamTokenizer.TT_EOL: 
			                System.out.println(""); 
			                System.out.println("Line No. : " + token.lineno()); 
			                break; 
						case StreamTokenizer.TT_NUMBER:
							System.out.println("Number : " + token.nval);
							break;
						case StreamTokenizer.TT_WORD:
							System.out.println("Word : " + token.sval);
							break;

			}
		}
			
		
 */

import java.io.*;

public class StreamTokanizerMethodDemo {
	@SuppressWarnings("unused")
    public static void main(String[] args)
			throws InterruptedException, FileNotFoundException, IOException {
		
		File myObj = new File("ABC.txt");
		FileWriter myWriter = new FileWriter("ABC.txt");
	    myWriter.write("Hello Everyone" + "\n" + "1 \n 2\n 3" + "By Myvizhi S" + "Here is the example program for Stream tokanizer methods" );
	    myWriter.close();
	    
	    
	    FileReader reader = new FileReader("ABC.txt");
		BufferedReader bufferread = new BufferedReader(reader);
		StreamTokenizer token = new StreamTokenizer(bufferread);
		
		// Use of eolIsSignificant() method 
		token.eolIsSignificant(true); 
		
		
		// Use of lineno() method  
        // to get current line no. 
        System.out.println("Line Number:" + token.lineno());
        
        // Use of commentChar() method
		token.commentChar('a');
		
	    boolean arg = true; 
	    token.eolIsSignificant(arg);
        
        
        //use of lowerCaseMethod() method
        boolean arg1 = true; 
        token.lowerCaseMode(arg1); 
		int t;
		while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) {
			switch (t) {
				case StreamTokenizer.TT_EOL: 
	                System.out.println("End of Line encountered."); 
	                break; 
				/*case StreamTokenizer.TT_EOL: 
	                System.out.println(""); 
	                System.out.println("Line No. : " + token.lineno()); 
	                break; */
				case StreamTokenizer.TT_NUMBER:
					System.out.println("Number : " + token.nval);
					break;
				case StreamTokenizer.TT_WORD:
					System.out.println("Word : " + token.sval);
					break;

			}
		}
	}
}