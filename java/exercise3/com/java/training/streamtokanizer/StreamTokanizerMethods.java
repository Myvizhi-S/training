package com.java.training.streamtokanizer;

/*
Requirement:
    Mention any five methods of stream tokenizer?
Solution:
Stream Tokenizer methods:
   void commentChar(int ch)
   void eolIsSignificant(boolean flag)
   int lineno()
   void lowerCaseMode(boolean fl)
   int nextToken()
 */
