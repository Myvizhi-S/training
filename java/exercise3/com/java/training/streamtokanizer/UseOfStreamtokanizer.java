package com.java.training.streamtokanizer;

/*
Requirement:
    Why do we use stream tokenizer?
Solution:
Using a Java StreamTokenizer you can move through the tokens in the underlying Reader. You do so by calling the nextToken() method of  the StreamTokenizer inside a loop. After each call to nextToken() the StreamTokenizer has several fields you can read to see what kind of token was read, it's value etc. These fields are:
    ttype   The type of token read (word, number, end of line)
    sval    The string value of the token, if the token was a string (word)
    nval    The number value of the token, if the token was a number.
 
 */
