package com.java.training.io;

/*
 Requirement: 
 To read the file contents  line by line in streams with example.
  
 Entity: 
 ReadFileStreamDemo 
  
 Method Signature: 
 public static void main(String[] args);
  
 Jobs to be done: 
   1. The path of the file is stored in filePath
   2. Read the file content in lines as stream.
   3. For each line
      3.1. Print the line.
 				  	
PseudoCode:
 
public class ReadFileStreamDemo {
	
	public static void main(String[] args) throws IOException {
		
		Path filePath = Paths.get("C:\\text.txt");
		Stream<String> stream = Files.lines(filePath) ;
        stream.forEach(System.out::println);		
	}
}

 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadFileStreamDemo {
	
	public static void main(String[] args) throws IOException {
		
		Path filePath = Paths.get("C:\\text.txt");
		Stream<String> stream = Files.lines(filePath) ;
        stream.forEach(System.out::println);
        stream.close();
	}
}