package com.java.training.io;

/*
Requirements:
To read a file using java.io.file

Entities:
ReadFileDemo

Functiondeclaration:
   public static void main(String[] args)
   
Jobs to be done:
1.In main method,try and catch is used
  1.1 Set the path of the file and use scanner() to get the input
  1.2 In while loop,read the file and print
2.Use close() method,to close the readingfile.

Pseudocode:
public class ReadFileDemo {
  public static void main(String[] args) {
    try {
      File file = new File("C:\\text.txt");//set the path to read the file
      Scanner scanner = new Scanner(file);//scanner() method
      while (scanner.hasNextLine()) {
        String string = scanner.nextLine();
        System.out.println(string);
      }
      scanner.close();
    } catch(Exception e){
        System.out.println(e);
  }
    }
  }

 */

import java.io.File; 
import java.util.Scanner;

public class ReadFileDemo {
    public static void main(String[] args) {
        try {
        	File file = new File("C:\\text.txt");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String string = scanner.nextLine();
                System.out.println(string);
            }
            scanner.close();
        } catch(Exception e){
            System.out.println(e);
        }
    }
}