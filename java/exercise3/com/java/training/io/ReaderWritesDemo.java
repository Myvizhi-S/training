package com.java.training.io;

/*
 Requirement: 
  Towrite a Java program reads data from a particular file using FileReader and writes
  it to another, using FileWriter
  
 Entity: 
 ReadAndWriteDemo
 
 Method Signature: 
 public static void main(String[] args);
  
 Jobs to be done: 
 1. Create a FileWriter named fileWriter to save the destination file path and initialize null.
 2. Create a FileReader named fileReader and initialize to null for the source file path.
 3. While the file has not reached end.
  	3.1) Read each characters and write it to destination file.
 4. Close the fileReader.
 5. Close the fileWriter.

 PseudoCode:
 
 public class ReaderWritesDemo {
	
	public static void main(String[] args) throws IOException {  
        FileInputStream reader = new FileInputStream("C:\\io\\text.txt");
        FileWriter writer= new FileWriter("C:\\io\\text1.txt");
        int data = reader.read();
        
        while (data != -1) {
            char dataChar = (char) data;
            writer.write(dataChar);
            data = reader.read();
        }
        
        System.out.println("Read and write operation was done successfully ");
        reader.close();
        writer.close();
    }
}
*/

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

public class ReaderWritesDemo {
    
    public static void main(String[] args) throws IOException {
        FileInputStream reader = new FileInputStream("C:\\io\\text.txt");
        FileWriter writer= new FileWriter("C:\\io\\text1.txt");
        int data = reader.read();
        
        while (data != -1) {
            char dataChar = (char) data;
            writer.write(dataChar);
            data = reader.read();
        }
        
        System.out.println("Read and write operation was done successfully ");
        reader.close();
        writer.close();
    }
}
