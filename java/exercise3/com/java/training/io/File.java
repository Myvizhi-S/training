package com.java.training.io;

/*
Requirements:
To read the file using inputstream

Entities:
The class named as File

FunctionDeclaration:
public static void main(String[] args)

Jobs to be done:
1) In main method,try and catch block is used.
  1.1) In First,Set the path to read the file and initialize i=0 in which type is int
  1.2) In while condition,reading the file until it gets false and print the result
2) After read the file,Use close()method to close the file.

Pseudocode:

public class File {

	public static void main(String[] args) {
                //create try and catch block
		 try {
		        FileInputStream file = new FileInputStream("C:\text.txt");//Fix the path to read the file 
		        int i = 0;
				while((i = file.read()) != -1) {
                    System.out.println((char)i);
				}
		        file.close();
		 } catch(Exception e) {
		            System.out.println(e);
		 }
	}
}
*/

import java.io.FileInputStream;

public class File {
	
    public static void main(String[] args) {
        try {
        	FileInputStream file = new FileInputStream("C:\\text.txt");
		    int i = 0;
		    while((i = file.read()) != -1) {
		    	System.out.println((char)i);
			}
		    file.close();
		} catch(Exception e) {
		    System.out.println(e);
		}
    }
}