package com.java.training.io;

/*
Requirements:
To write a string content using Writer

Entities:
WriteStringDemo

Functiondeclaration:
   public static void main(String[] args)
   
Jobs to be done:
1.In main method,try and catch block is used
  1.1 Set the path of the file and create a string named as content
  1.2 write the content to the file. 
  1.3 Use close() method,to close the file.
2.print the output.

Pseudocode:
public class WriteStringDemo {
   public static void main(String[] args) {
//create try and catch block
		 try {  
	            Writer writer = new FileWriter("C://text.txt"); //set the path  
	            String string = "Hello everyone";  //create a string
	            writer.write(string);  
	            writer.close();  
	            System.out.println("Finish");  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	}

}
*/

import java.io.Writer;
import java.io.FileWriter;

public class WriteStringDemo {
   public static void main(String[] args) {
		 try {  
	            Writer writer = new FileWriter("C://text.txt");  
	            String string = "Hello everyone";  
	            writer.write(string);  
	            writer.close();  
	            System.out.println("Finish");  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	}

}