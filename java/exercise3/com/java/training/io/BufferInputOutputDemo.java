package com.java.training.io;

/*
 Requirement:
   To write and read a file using filter and buffer input and output streams.
 
 Entity:
   BufferInputOutputDemo
 
 Method Signature:
   publictic void main(String[] args)
  
 Jobs to be Done:
  		1)Create a file to write a file using BufferOutputStream
 		2)Write a message in that file.
  		3)Access the file using BufferrInputStream
  
 Pseudo Code:
  		class BufferInputOutputDemo {
  			public static void main(String[] args) {
  				//create a file to write a new message.
  
  				//Access that file in FileOutputStream 
  				FileOutputStream file = new FileOutputStream(data);
  	
  				bufferedOutputStream filter = new BufferedOutputStream(file);
  
  				//Add some message to the string variable.
  
  				//Add that particular message to that file.
  
  
  				//To Read that file access it using FilterInputStream
     		    FileInputStream  file1 = new FileInputStream(data1);  
        		BufferedInputStream filter1 = new BufferedInputStream(file1); 
        		//Print the message that has been written
  			}
  		}
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;

public class BufferInputOutputDemo {
	
    public static void main(String[] args) throws IOException {
		File data = new File("C:\\io\\text.txt");
		FileOutputStream file = new FileOutputStream(data);

		BufferedOutputStream bout = new BufferedOutputStream(file);
		String string = "welcome to java class";
		byte b[] = string.getBytes();
		bout.write(b);
		bout.flush();
		bout.close();



		File data1 = new File("C:\\io\\text1.txt");
		FileInputStream file1 = new FileInputStream(data1); 
		@SuppressWarnings("unused")
        FilterInputStream filter1 = new BufferedInputStream(file1);

		try {

			BufferedInputStream bin = new BufferedInputStream(file1);
			int index;
			
			while ((index = bin.read()) != -1) {
				System.out.print((char) index);
			}
			
			bin.close();

		} catch (Exception e) {
			System.out.println(e);
		}


	}
}