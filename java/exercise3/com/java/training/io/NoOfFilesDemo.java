package com.java.training.io;

/*
Requirements : 
	Number of files in a directory and number of directories in a directory.
 
Entities :
	NoOfFilesDemo
  
Method Signature :
	public static void main(String[] args)
  
Jobs To Be Done:
	1)Create a reference for File with file as constructor argument.
	2)Print the number of files in the directory
	3)Add all the files and directories to the file array.
	4)initialize a integer variable with 0.
	5)for each item in the array
      		5.1)check whether the item is directory
			5.2)if yes, add 1 to the integer variable
  	6)Print the value of integer variable.
 
PseudoCode:
  
class NoOfFilesDemo {
 
	public static void main(String[] args) {
        File file = new File("C:\\io\\text.txt");
        
        File[] files = file.listFiles();
        int noOfDir = 0;
        int noOfFiles = 0;
        
        for(File element : files) {
            noOfDir += element.isDirectory() ? 1 : 0;
            noOfFiles += element.isFile() ? 1 : 0;
        }
        
        System.out.println("Number of directories in this directory : " + noOfDir);
        System.out.println("Number of files in this directory : " + noOfFiles);
    }

}

 */

import java.io.File;

public class NoOfFilesDemo {

	public static void main(String[] args) {
		File file = new File("C:\\io");
		
		File[] files = file.listFiles();
		int noOfDir = 0;
		int noOfFiles = 0;
		
		for(File element : files) {
			noOfDir += element.isDirectory() ? 1 : 0;
			noOfFiles += element.isFile() ? 1 : 0;
		}
		
		System.out.println("Number of directories in this directory : " + noOfDir);
		System.out.println("Number of files in this directory : " + noOfFiles);
	}

}
