package com.java.training.io;

/*
 * Requirement:
 * 		To write and read a file using filter and buffer input and output streams.
 * 
 * Entity:
 * 		FilterinputOutputDemo
 * 
 * Method Signature:
 * 		public static void main(String[] args)
 * 
 * Jobs to be Done:
 * 		1)Create a file to write a file using fileOutputStream
 * 		2)Write a message in that file.
 * 		3)Access the file using FilterInputStream
 * 
 * Pseudo Code:
 * 		class FilterInputOutputDemo {
 * 			public static void main(String[] args) {
 * 				//create a file to write a new message.
 * 
 * 				//Access that file in FileOutputStream 
 * 				FileOutputStream file = new FileOutputStream(data);
 * 	
 * 				FilterOutputStream filter = new FilterOutputStream(file);
 * 
 * 				//Add some message to the string variable.
 * 
 * 				//Add that particular message to that file.
 * 
 * 
 * 				//To Read that file access it using FilterInputStream
 *    		    FileInputStream  file1 = new FileInputStream(data1);  
        		FilterInputStream filter1 = new BufferedInputStream(file1); 
        		//Print the message that has been written
 * 			}
 * 		}
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

public class FilterInputOuputDemo {
	
    public static void main(String[] args) throws IOException {
		File data = new File("C:\\io\\text.txt");  
        FileOutputStream file = new FileOutputStream(data);  
        FilterOutputStream filter = new FilterOutputStream(file);  
           
        String s="Welcome to java";
       
        byte byte1[]=s.getBytes();      
        filter.write(byte1);     
        filter.flush();  
        filter.close();  
        file.close();
        
        File data1 = new File("C:\\io\\text1.txt");  
        FileInputStream  file1 = new FileInputStream(data1);  
        FilterInputStream filter1 = new BufferedInputStream(file1);  
       
        int index = 0;  
        
        while((index = filter1.read()) != -1){  
            System.out.print((char)index);  
        }  
        
        file1.close();  
        filter1.close(); 
	}
}
