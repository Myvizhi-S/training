package com.java.training.io;

/*
Requirements : 
		Write some String content using Writer.

Entities :
		StringWriterDemo

Method Signature :
		public static void main(String[] args)

Jobs To Be Done:
		1.Create a reference for FileReader with file as constructor argument.
    	2.Till the end of the file
         2.1)Read the content of the file.
         2.2)Print the content of the file.
    	3.Close the created input stream.

PseudoCode:

		class StringWriterDemo {
			public static void main(String args[]) throws Exception {
				Writer write = new FileWriter("C:\\io\\text.txt");
				String content = "The file was written successfully.";
				//Add the content to the file and close it.
				System.out.println("success");
			}
		}
*/

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class StringWriterDemo {

	public static void main(String[] args) throws IOException {
		Writer write = new FileWriter("C:\\io\\text.txt");
		String content = "The file was written successfully.";
		write.append(content);
		write.close();
		System.out.println("success");
		
	}

}
