package com.java.training.io;

/*
Requirements:
To write some String content using outputstream

Entities:
OutputStreamDemo

Functiondeclaration:
   public static void main(String[] args)
   
Jobs to be done:
1.In main method,throws IOException is used
  1.1 Set the path of the file and create odject as fos
  1.2create string named as s
  1.3 Get the string in byte format and write to the file
2.Use close() method,to close the file.
3.Print the output

Pseudocode:

public class OutputStreamDemo {

	public static void main(String[] args) throws IOException {
		 OutputStream fos = new FileOutputStream("C:\\text.txt");//set the path of file
			String string = "Hello Welcome to Java class";
			byte[] b = string.getBytes();//converting string into byte array    
			
	                fos.write(b);
			fos.close();
			System.out.println("Success");
	        }
*/

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamDemo {

    public static void main(String[] args) throws IOException {
        OutputStream fos = new FileOutputStream("C:\\io\\text.txt");
        String string = "Hello Welcome to Java class";
        
        byte[] inputStream = string.getBytes();
        fos.write(inputStream);
        fos.close();
        System.out.println("Successfully done");
    }
}