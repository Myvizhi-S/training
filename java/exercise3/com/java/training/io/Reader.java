package com.java.training.io;

/*
Requirements:
To read the file using Reader

Entity:
Reader

Function declaration:
   public static void main(String[] args)
   
Jobs to be done:
1.In main method,throws Exception is used
  1.1 Set the path of the file and declare the variable as i
  1.2 In while loop,read the file until loop gets false
  1.3 Print the result
2.Use close() method,to close the readingfile.

Pseudocode:

public class Reader {
           // throws Exception
	public static void main(String[] args)throws Exception{    
		          FileReader fr=new FileReader("C:\\text.txt"); //set the path to read the file   
		          int i;    
		          while((i=fr.read())!=-1)    
		          System.out.print((char)i);    
		          fr.close();    
		    }    
}
*/

import java.io.FileReader;

public class Reader {

	public static void main(String[] args)throws Exception {    
		          FileReader file = new FileReader("C:\\text.txt");    
		          int i;    
		          while((i = file.read()) != -1) {
		        	  System.out.print((char)i);
		          } 
		          file.close();
    }    
}