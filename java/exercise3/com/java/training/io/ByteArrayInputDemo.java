package com.java.training.io;

/*
 *Requirement:
 * 		To write a program for ByteArrayInputStream class to read byte array as input stream.
 * 
 * Entity:
 * 		ByteArrayInputDemo
 * 
 * Method Signature:
 * 		public static void main(String[] args) 
 * 
 * Jobs To be Done:
 * 		1)Create a numbers to be added to the ByteArray system array.
 * 		2)Add those numbers to the byte array input stream system.
 * 		3)It access the array as byte read and process the required operation for it.
 * 
 * Pseudo Code:
 * 		class ByteArrayInputDemo {
 * 			public static void main(String[] args) {
 * 				byte[] byt = {"Some numbers"};
 * 
 * 				//Assign the values to the byte input stream
 * 				ByteArrayInputStream bis = new ByteArrayInputStream(byt);  
 *  
 *  			//print the each value of input value into the special characters by reference of ascii code.
 * 			}
 * 		}
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ByteArrayInputDemo {
	 
    public static void main(String[] args) throws IOException {  
		    byte[] byt = { 35, 36, 37, 38 };  
		    // Create the new byte array input stream  
		    ByteArrayInputStream bis = new ByteArrayInputStream(byt);  
		    int index = 0;  
		    
		    while ((index = bis.read()) != -1) {  
		      //Conversion of a byte into character  
		      char character = (char) index;  
		      System.out.println("ASCII value of Character is:" + index + "Special character is: " + character);  
		    }  
    }  
}
