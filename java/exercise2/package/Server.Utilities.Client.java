/*Requirement:
    To assume that you have written some classes.Belatedly,you decide that they should be split into
    three packages, as listed in the table below. Furthermore, assume that the classes are
    currently in the default package (they have no package statements).

    Package Name    Class Name
    mygame.server   Server
    mygame.shared   Utilities
    mygame.client   Client
    1.What line of code will you need to add to each source file to put each class in the right
    package?
    2.To adhere to the directory structure,you will need to create some subdirectories in your
    development directory,and put source files in the correct subdirectories.What subdirectories
    must you create? Which subdirectory does each source file go in?
    3.Do you think you'll need to make any other changes to the source files to make them compile
    correctly? If so, what?

Entity:
    Server,Utilities,Client
    
Function declaration:
    No function
    
Jobs to be done:
    1.Consider the given question.
    2.Define the package statement for the given package name and class.
    3.Declare the subdirectories for the source files.
    4.Define the import statement in the source file.
*/

Answer:
    1.At first line of the code, give as follows
        package mygame.server;
        package mygame.shared;
        package mygame.client;
        
    2.Create server,shared,client as subdirectories.
      Server.java in server
      Utilities.java in shared
      Client.java in client
      
    3.Import the source files to compile,like
        import mygame.server.Server;
        import mygame.shared.Utilities;
        import mygame.client.*;