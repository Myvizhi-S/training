/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects
    
Entities:
    InheritanceDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be done:
    1. Declare the class InheritanceDemo.
    2. call the function public static void main(String[] args).
    3. Intantiate  the classes Animal, Dog, Cat and Snake.
    4. Inheritance, using Animal, Dog, Cat and Snake class of objects is done.
*/
public class InheritanceDemo{
    public static void main(String[] args){
        Animal animal = new Animal();
        Dog dog = new Dog();
        Cat cat = new Cat();
        Snake snake = new Snake();
        animal.sound();
        animal.run();
        animal.run(7);
        cat.sound();
        cat.run();
        cat.run(6);
        dog.sound();
        dog.run();
        dog.run(8);
        snake.sound();
        snake.run();
        snake.run(9);
    }
}