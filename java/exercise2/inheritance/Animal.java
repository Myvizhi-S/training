/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entities:
    Animal

Function Declaration:
    public void sound()
    public void run()
    public void run(int kilometer)

Jobs to be Done:
    1. Declare the class Animal
    2. Declare the function public void sound() and print the statement.
    3. Declare the function run and run(int kilometers) to show the examples for overloading.*/

public class Animal {
    public void sound() {
        System.out.println(" This is the parent class");
    }
    
    public void run() {
        System.out.println("the animal is running");
    }
    
    public void run(int kilometer) {
        System.out.println("the animal runs without any tired");
    }
}