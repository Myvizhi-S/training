/*Requirement:
    By considering the following two classes, to find method which overrides and hides a method in the superclass and the purpose of other methods.
    public class ClassA {
        public void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public static void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }

    public class ClassB extends ClassA {
        public static void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }

Entities:
    ClassA
 
    ClassB

Function declaration:
        public static void methodOne(int i)
        
        public void methodTwo(int i)
        
        public void methodThree(int i)
        
        public static void methodFour(int i)
        

Jobs to be done:
    Find method which overrides and hides a method in the superclass and the purpose of other methods in the given two classes.
*/

ANSWERS:
    a)methodTwo overrides a method in the superclass 
    b)methodFour hides a method in the superclass 
    c)The other methods will cause the compilation errors