/*
Requirement:
    To find Integer method can you use to convert an int into a string that expresses the number in
    hexadecimal.

Entity:
    IntegerMethodDemo.

Function Declaration:
    public static void main(String[] args)
    toHexString()
    toUpperCase()

Jobs to be Done:
    1. Import the package java.util.Scanner.
    2. Declare the class IntegerMethodDemo.
    3. Under a main method get a scanner input for the variable of type int which is to be converted
       into string of hexadecimal number.
    4. The method to be used is Integer.toHexString().
*/

import java.util.Scanner;
public class IntegerMethodDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        System.out.println("HexaDecimal representation is = " + Integer.toHexString(number));
    }
}