Use the Java API documentation for the Box class (in the javax.swing package) to help you answer the following questions.

Question: What static nested class does Box define?

Answer: Box.Filler

Question: What inner class does Box define?

Answer: Box.AccessibleBox

Question: What is the superclass of Box's inner class?

Answer: [java.awt.]Container.AccessibleAWTContainer

Question: Which of Box's nested classes can you use from any class?

Answer: Box.Filler

Question: How do you create an instance of Box's Filler class?

Answer: new Box.Filler(minDimension, prefDimension, maxDimension)


What methods would a class that implements the java.lang.CharSequence interface have to implement?
Answer 1: charAt, length, subSequence, and toString.


What Integer method can you use to convert an int into a string that expresses the number in hexadecimal? For example, what method converts the integer 65 into the string "41"?

Answer: toHexString


What Integer method would you use to convert a string expressed in base 5 into the equivalent int? For example, how would you convert the string "230" into the integer value 65? Show the code you would use to accomplish this task.

Answer: valueOf. Here's how:

String base5String = "230";
int result = Integer.valueOf(base5String, 5);


What Double method can you use to detect whether a floating-point number has the special value Not a Number (NaN)?
 isNaN