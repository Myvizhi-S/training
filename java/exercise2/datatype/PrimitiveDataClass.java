/*
Requirement:
    To print the Class names of  all the primitive datatype.

Entity:
    PrimitiveDataClass

Function declaration:
    public static void main(String[] args)
    getName()

Jobs to be done:
    1.Create the class PrimitiveDataClass
    2.Declare a variable to store the class name by using getName() method.
    3.Use it for various data type.
    4.Print the class names.
*/

public class PrimitiveDataClass {
    public static void main(String[] args) {
        String className1 = int.class.getName();
        System.out.println("Class Name of Integer : " + className1);

        String className2 = char.class.getName();
        System.out.println("Class Name of Character : " + className2);

        String className3 = double.class.getName();
        System.out.println("Class Name of Double : " + className3);

        String className4 = float.class.getName();
        System.out.println("Class Name of Float : " + className4);
    }
}