/*
Requirement:
    To find the value of the following expression, and to say why
     Integer.valueOf(1).equals(Long.valueOf(1)).

Entity:
    FindValue

Function Declaration:
    public static void main (String[] args).
    valueOf().

Jobs to be Done:
    1. To read the given question and to say the what value comes as output.
    2. Declare the class FindValue.
    3. Under the main statement print the value of the given expression.
   Answer of the given expression is false. this is prooved in the below program. Because the two
   objects Integer and Long have different types.
*/

public class FindValue {

    public static void main(String[] args) {
        System.out.println(Integer.valueOf(1).equals(Long.valueOf(1)));
    }
}