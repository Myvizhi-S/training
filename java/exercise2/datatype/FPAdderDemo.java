/*
Requirement:
    To create a program that is similar to the previous one but instead of reading integer arguments,
    it reads floating-point arguments.It displays the sum of the arguments, using exactly two digits
    to the right of the decimal point.

Entity:
    FPAdderDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1.Import the package java.text.DecimalFormat.
    2.Declare the class FPAdderDemo.
    3.Under a main method declare the variable numArgs to store length of the arguments.
    4.If the length of the arguements is more than two then print the sum of all the arguements.
    5.If the length of the arguements is less than two show the error message.
    5.Now format the output value.
*/

import java.text.DecimalFormat;

public class FPAdderDemo {
    public static void main(String[] args) {
        int numArgs = args.length;
        if (numArgs < 2) {
            System.out.println("Error");
        } else {
        double sum = 0.0;

        for (int i = 0; i < numArgs; i++) {
                sum += Double.valueOf(args[i]).doubleValue();
        }

        DecimalFormat myFormatter = new DecimalFormat("###,###.##");
        String outputValue = myFormatter.format(sum);
            System.out.println(outputValue);
        }
    }
}