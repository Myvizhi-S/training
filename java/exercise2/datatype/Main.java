/*
Requirement:
    To print the type of the result value of following expressions
    - 100 / 24
    - 100.10 / 10
    - 'Z' / 2
    - 10.5 / 0.5
    - 12.4 % 5.5
    - 100 % 56

Entity:
    Main

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1.Create a class Main.
    2.Print the type of the result value of expressions
    - 100 / 24
    - 100.10 / 10
    - 'Z' / 2
    - 10.5 / 0.5
    - 12.4 % 5.5
    - 100 % 56
    by using getClass() and getName() inside the print statement.
*/

public class Main
{
    public static void main(String[] args) {
        Object o = 100 / 24;
        System.out.println(o.getClass().getName()); //java.lang.Integer
        Object a = 100.10 / 10;
        System.out.println(a.getClass().getName()); //java.lang.Double
        Object b = 'Z' /2;
        System.out.println(b.getClass().getName()); //java.lang.Integer
        Object c =10.5 /0.5 ;
        System.out.println(c.getClass().getName()); //java.lang.Double
        Object d = 12.4 % 5.5;
        System.out.println(d.getClass().getName()); // java.lang.Double
        Object e = 100/56;
        System.out.println(e.getClass().getName()); //java.lang.Integer
    }
}