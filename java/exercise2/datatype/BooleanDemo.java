/*
Requirement:
    To invert the value of a boolean using an operator.

Entity:
    BooleanDemo

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1.Create a class BooleanDemo.
    2.Check two strings are equal or not.
    3.Invert the value of boolean using loical complement operator.
    4.Print the inverted boolean value.
*/

public class BooleanDemo {
    public static void main(String[] args) {
        boolean word = !("world" == "world");   //logical complement operator(!)is used to invert
        System.out.println("Inverted boolean : " + word);
    }
}