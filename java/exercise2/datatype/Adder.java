/*
Requirement:
  To create a program that reads an unspecified number of integer arguments from the command line and adds them together.
  
Entities:
  Adder

Function Declaration:
   public static void main(String[] args)
   
Jobs To Be Done:
  1. Create a class Adder.
  2. Call the method public void add(int... array).
  3. Inside the method add using if statement.
  4. Call the main function. 
*/
public class Adder {

    public void add(int... array) {
        if(array.length == 1) {
            System.out.println("Add more numbers");
        } else {
            int sum = 0;
            for(int number : array) {
                sum += number;
            }
            System.out.println(sum);
        }
    }
    public static void main(String[] args) {
    Adder adder = new Adder();
    adder.add(1,2,3,10);
    }
}