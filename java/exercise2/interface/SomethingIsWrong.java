/*
Requirement:
    To identify what is wrong in the given interface program.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Entity:
    SomethingIsWrong

Function Declaration:
    void aMethod(int aValue)

Jobs to be done:
    1. Look at the following program and identify the error.
    2. After identifing the error Correct the program.
    3. Method declaration is wrong in this program. The correct format to declare the method is by using default in it.
*/
public interface SomethingIsWrong {
    default void aMethod(int aValue){     // Here default can be used.
        System.out.println("Hi Mom");
    }
}