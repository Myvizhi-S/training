/*
Requirement:
    To analyze the following interface valid or not:
    public interface Marker {}

Entity:
    No Entity.

Function Declaration:
    No function declaration.

Jobs To Be Done:
    Analyse the given expression and say whether it is valid or no.
*/
Answer:
    This interface is valid.