/*
Requirement:
    To Write a class that implements the CharSequence interface found in the java.lang package.
    And the implementation should return the string backwards.

Entity:
    InterfaceDemo.
    CharSequence.

Function Declaration:
    toCharArray()
    clone().
    length()
    subSequence()
    toString()
    charAt()
    public static void main(String[] args)

Jobs to be Done:
    1. Import CharSequence from java.lang.package.
    2. Declare a class InterfaceDemo implement CharSequence.
    3. Declare a varaiable charSequence of type String.
    4. Give a constructor.
    5. Get the string and store it in strArray of char[] type.
    6. Take a clone of the strArray and upto a length of the String reverse the string and print it.
    7. Now code for the given question using length(), charAt(), subSequence() and toString().
    8. Now print the results.
*/

import java.lang.CharSequence;
public class InterfaceDemo implements CharSequence {

    private String charSequence;

    public InterfaceDemo(String charSequence) {
    char[] strArray = charSequence.toCharArray();
    char[] reversedArray = strArray.clone();
    int j = strArray.length - 1;
    for (int i = 0; i < strArray.length; i++) {
        reversedArray[j] = strArray[i];
        j--;
    }
    this.charSequence = new String(reversedArray);
    System.out.println("Reverse of the String is" + " " + this.charSequence);
    }

    public int length() {
        return charSequence.length();
    }

    public char charAt(int i) {
        return charSequence.charAt(i);
    }

    public CharSequence subSequence(int i, int i1) {
        return charSequence.subSequence(i, i1);
    }

    public String toString() {
        return charSequence;
    }

    public static void main (String[] args) {
        String sentence = "Select one of the sentences";
        InterfaceDemo sentence1 = new InterfaceDemo(sentence);
        System.out.println("length" + " " + sentence1.length());
        System.out.println("charAt" + " " + sentence1.charAt(8));
        System.out.println("subSequence = " + " " + sentence1.subSequence(4, 10));
        System.out.println("toString" + " " + sentence1.toString());
    }
}