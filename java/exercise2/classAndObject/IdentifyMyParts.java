/*Requirement:
    To find the output of the given program.
    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
    
Entities:
    IdentifyMyParts

Function Declaration:
    There is no function to be declared in this program.

Jobs To Be Done:
    1)Consider the given program
    2)Check the x & y values of objects a and b.
    3)Print the resultant values of each variable.
    4)Identify the part of x and print the result
*/
OUTPUT:
a.y = 5
b.y = 6
a.x = 2
b.x = 2
IdentifyMyParts.x = 2