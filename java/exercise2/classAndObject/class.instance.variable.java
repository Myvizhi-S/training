/*Requirement:
    To find the class and instance Variables in the given program.
public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
Entities:
    IdentifyMyParts

Function Declaration:
    There is no function declared in this program.

Jobs To Be Done:
    1)Considering the given class from the question.
    2)Finding the class variable(declared with static) and instance variable.
    3)Answering it for the question.
*/

Class Variable : x
Instance Variable : y