/*Requirement:
    To correct the error for the given program
 public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
Entities:
    SomethingIsWrong

Function Declaration:
    area(),public static void main(String[] args)

Jobs To Be Done:
    1)Considering the given program.
    2)Finding the error in the program.
    3)It Doesn't have the object creation.
    4)To run the program correctly Change the class name from SomethingIsWrong to Rectangle.
    5)Declaring the property with int type and also declaring the object
    6)To print the data, Create the method area() and return the product of width and height.
*/
THE CORRECTED PROGRAM:
public class Rectangle {
    public int width;
    public int height;
    public  int area(){
        return (width * height);
    }
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
    }
}