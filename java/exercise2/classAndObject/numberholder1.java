/*Requirement:
    To write the code that creates an instance of the class and initializes its two member variables with provided values,and then displays the value of each member variable.
    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }

Entities:
    NumberHolder

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)Consider the given program.
    2)Declare the main function inside the class.
    3)Create the object numberHolder for the class NumberHolder.
    4)Initialize the value for the variables( 3 for int and 4.2f for float)
    5)Display the instance variable values by printing the values.

*/
public class NumberHolder {
    public int anInt;
    public float aFloat;
    public static void main(String[] args) {
    NumberHolder numberHolder = new NumberHolder();
    numberHolder.anInt = 3;
    numberHolder.aFloat = 4.2f;
    System.out.println(numberHolder.anInt);   //3
    System.out.println(numberHolder.aFloat);   //4.2
    }
}