/*
Requirement:
    To find fibonacci series using Recursion.

Entity:
   FibonacciRecursion

Function Declaration:
    public static void printFibonacci(int i),public static void main(String[] args)

Jobs to be Done:
    1. Import Scanner and declare the class FibonacciRecursion.
    2. Declare first two variables as a and b and assign the value to the variables as 1 and 0 and declare the third variable sum.
    3. Declare the function as public static void printFibonacci(int i) and give the body of
       the function.
    4. Call the function printFibonacci(int i) by declaring the variable n.
    5. Print the series.
*/

import java.util.Scanner;
public class FibonacciRecursion {
        public static int a = 0;
        public static int b = 1;
        public static int sum;
        public static void printFibonacci(int i) {
            if(i > 0) {
                sum = a + b;
                a = b;
                b = sum;
                System.out.print(a + " ");
                printFibonacci(i - 1);
            }
        }
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        printFibonacci(n);
    }
}