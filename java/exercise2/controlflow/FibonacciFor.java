/*
Requirement:
    To find fibonacci series using for loop.

Entity:
    FibonacciFor

Function Declaration:
    public static void main(String[] args) 

Jobs to be Done:
    1. Import Scanner and declare the class FibonacciFor.
    2. To print the series declare the range and assign the first two numbers of
       the series under two different variables as a and b.
    3. Use for loop and assign the value to the new variable i.The process should be continued untill i <= n.
    4. Add the values of a and b and store it in new variable sum.
    5. Assign the value of b to a and b to sum.
    6. Print  the output a.
*/

import java.util.Scanner;
public class FibonacciFor {
    public static void main(String[] args) {
        int n;
        int a = 0;
        int b = 1;
        int sum;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        for(int i = 1; i <= n; i++) {
            sum = a + b;
            a = b;
            b = sum;
            System.out.print(a + " ");
         }
    }
}