/*
Requirement:
    To write the test program consists of following codes snippet and make the value of aNumber
    to 3 and find the output of the code by using proper line space, breakers and curly braces.
    The given snippet is as follows:
     if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
     else System.out.println("second string");
     System.out.println("third string");

Entities:
        Check

Function Declaration:
         public static void main(String[] args)

Jobs To Be Done:
    1. Import Scanner and the create a class Check.
    2. Create a variable aNumber of type int.
    3. Assign the value 3 for the variable aNumber.
    4. Proper line space and curly braces should be used in the given snippet.
    5. Check the output.
*/

import java.util.Scanner;
public class Check {
    public static void main(String[] args) {
        int aNumber;
        Scanner sc = new Scanner(System.in);
        aNumber = sc.nextInt();
        if(aNumber >= 0) {

            if(aNumber == 0) {
                System.out.println("First String");
            }
         }
        
        else {
            System.out.println("Second String");
        }
        System.out.println("Third String");
    }
}