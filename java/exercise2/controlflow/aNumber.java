/*Requirement:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
  There is no class name in this program.

Function Declaration:
  There is no function to be declared in this program.

Jobs To Be Done:
  1)Consider the given program
  2)Check the aNumber value is greater than or equal to zero using the If statement.
  3)It is true, So moves to the next If statement.
  4)Check the aNumber value is equal to zero 
  5)It is false, So it skips the step and move to the else part.
  6)There is no else part in this if staatement. So it moves to the else of previous if statement.
  7)It prints the output second string and followed by third string.
*/
OUTPUT:
second string
third string