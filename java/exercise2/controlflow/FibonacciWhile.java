

/*
Requirement:
    To find fibonacci series using while loop.

Entity:
    FibonacciWhile

Function Declaration:
     public static void main(String[] args)

Jobs to be Done:
    1. Import Scanner and Declare the class FibonacciWhile.
    2. Declare the range for the Series to be printed and assign the first two numbers of
       the series by two different variables as a and b.
    3. Use while loop and assign the value to the new variable i. Continue the process untill i <= n.
    4. Add the values of a and b and store it in new variable sum.
    5. Assign the value of b to a and the value of b to sum and then increments the value of i.
    6. Print the output a.
*/

import java.util.Scanner;
public class FibonacciWhile {
    public static void main(String[] args) {
        int n, a, b, i;
        int sum;
        a = 0;
        b = 1;
        i = 1;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        while(i <= n) {
            sum = a + b;
            a = b;
            b = sum;
            i++;
            System.out.print(a + " ");
        }
    }
}