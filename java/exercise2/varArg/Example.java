/*Requirement:
  To demonstrate overloading with varArgs.
  
Entities:
  Example

Function Declaration:
   vaTest is declared.
   
Jobs To Be Done:
  1. Create a class named Example.
  2. create a function named test with different parameters
  3. call the functions in the main.
*/

public class Example {
   static void vaTest(int ... no) {
      System.out.print(
         "vaTest(int ...): " + "Number of args: " + no.length +" Contents: ");
      for(int n : no)System.out.print(n + " ");
      System.out.println();
   }
   static void vaTest(boolean ... bl) {
      System.out.print(
         "vaTest(boolean ...) " + "Number of args: " + bl.length + " Contents: ");
      for(boolean b : bl)System.out.print(b + " ");
      System.out.println();
   }
   static void vaTest(String msg, int ... no) {
      System.out.print(
         "vaTest(String, int ...): " + msg +"no. of arguments: "+ no.length +" Contents: ");
      for(int  n : no)
      System.out.print(n + " ");
      System.out.println();
   }
   public static void main(String args[]) {
      vaTest(4, 5, 6);
      vaTest("Testing: ", 40, 50);
      vaTest(true, false, false);
   }
}