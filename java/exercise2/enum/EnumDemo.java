/*Requirement:
  To compare the enum values using equal method and == operator.
  
Entities:
  EnumDemo

Function Declaration:
  public static void main(String[] args) 
   
Jobs To Be Done:
  1. Declare enum Months
  2. Create a class EnumDemo
  3. Inside the class call the main function.
  4. If and else if statements are used inside the function to find the number of days in a month.
  5. Compare the enum values with equal method and comparision operator.
*/

public class EnumDemo {
   
    public static void main(String[] args) {
       
          Months month = Months.valueOf("MAY");
      
        if(month.equals (Months.JANUARY)) {
            
            System.out.println("JANUARY has 31 days");
        
        }else if(Months.FEBRUARY == month) {
           
           System.out.println("FEBRUARY has 29 days");
        
        } else if(Months.MARCH == month) {
           
           System.out.println("MARCH has 31 days");
        
        }else if(month.equals(Months.APRIL)) {
            
            System.out.println("APRIL has 30 days");
       
       }else if(Months.MAY == month) {
        
        System.out.println("MAY has 31 days");
        
       }       
    }
}
