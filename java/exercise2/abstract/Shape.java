public abstract class Shape{
    public abstract void printArea();
    public abstract void printPerimeter();
}