public class Circle extends Shape {
    private double radius;
    
    public Circle(double radius) {
        this.radius = radius;
    }
    
    public void printArea() {
        System.out.println("area of circle is = " + 3.14 * radius * radius);
    }
    
    public void printPerimeter() {
        System.out.println("perimeter of circle = " + 2 * 3.14 * radius);
    }
}
