public class Square extends Shape {
    private double side;

    public Square(double side) {
        this.side = side;
    }

    public void printArea() {
        System.out.println("area of square = " + side*side);
    }

    public void printPerimeter() {
        System.out.println("perimeter of square = " + 4 * side);
    }
}
