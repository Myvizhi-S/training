/*
Requirement:
    To demonstrate the abstract class using the class shape that has two methods to calculate the
    area and perimeter of two classes named the Square and the Circle extended from the class Shape.

Entity:
    AbstractDemo 
    Shape.
    Square.
    Circle.

Function Declaration:
    public void printArea();
    public void print Perimeter();
    public static void main(String[] args).

Jobs to be done:
    1) declare a abstract class called Shape
    2) extends other class like square,circle from shapes 
    3) In each inherited class declare the same methode used in abstract class 
       without abstract keyword and write the print statement inside it
    4)Using scanner get inputs that is radius and side from user
    5)declare a main class as AbstractDemo and call the function 
      using object of circle and square 
    
*/

import java.util.Scanner;
public class AbstractDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double side = scanner.nextDouble();
        double radius = scanner.nextDouble();
        Square square = new Square(side);
        square.printArea();
        square.printPerimeter();
        Circle circle = new Circle(radius);
        circle.printArea();
        circle.printPerimeter();
    }
}
