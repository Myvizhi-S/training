/*Requirement:
    To write a program that computes your initials from your full name and displays them.

Entity:
    Initials

Function declaration:
    public static void main(String[] args),isUpperCase(),length(),append()

Jobs to be done:
    1)Declare the class as Initials.
    2)Declare the variables.
    3)Assign values to the variables.
    4)Use for loop to get my initials.
*/

public class Initials {
    public static void main(String[] args) {
        String name = "Yamala. Pavithra";
        StringBuffer initials = new StringBuffer();
        int length = name.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(name.charAt(i))) {
                initials.append(name.charAt(i));
            }
        }
        System.out.println("My initials are: " + initials);
    }
}
