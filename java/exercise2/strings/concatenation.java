/*Requirement:
    To show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";
    
Entities:
    There is no entity in this program.

Function:
    There is no function to be declared in this program.
Jobs to be done:
    1) We can concatenate by using the addition operator.
    2) We can concatenate by  using concat() function.
*/
SOLUTION:
    1) hi + mom
    2) hi.concat(mom)