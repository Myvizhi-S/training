/*Requirement:
    To find the references to those objects exist after the code executes in the following code creates one array and one string object and
 also find object is either eligible for garbage collection.
...
String[] students = new String[10];
String studentName = "Peter Smith";
students[0] = studentName;
studentName = null;
...

Entity:
    No Entity declared in this program.

Function declaration:
    No function declared in this program.

Jobs to be done:
    Find the references to those objects exist after the code executes and also find object is either eligible for garbage colection in the given program.
*/

Explanation:
    1)There is one reference to the students array and that array has one reference to the string 
      Peter Smith.
    2)Neither object is eligible for garbage collection.
    3)The array students is not eligible for garbage collection because it has one reference to the object studentName even though that object 
      has been assigned the value null.
    4)The object studentName is not eligible either because students[0] still refers to it.