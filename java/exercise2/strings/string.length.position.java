*Requirement:
    To answer the following questions.
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.

Entities:
    There is no entity in the given program.
    
Function Declartion:
    There is no function to be declared in the given program.
    
Jobs to be done:
    1)Find the value of hannah.length().
    2)Find the value of chArat(12).
    3)Find the postion of letter "b".

Solution:
    1)32
    2)e
    3)hannah.charAt(15)