/*Requirement:
    To find the initial capacity of the following string builder
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");

Entity:
    There is no entity here..
    
Function declaration:
    There is no function to be declared.
    
Jobs to be done:
    1)Find the initial capacity of the string.It is the length of that string and add 16 in it.
*/
SOLUTION:
    Length of the given String = 26
    Initial capacity of the string = 26 + 16 = 42.