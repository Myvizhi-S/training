/*Requirement:
    TO sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Entity:
    SortArray
    
Function declaration:
    public static void main(String[] args),sort(), toUpperCase(),toString()
    
Jobs to be done:
    1)Import Arrays and declare the class SortAarray.
    2)Create an array using given places s strings.
    3)Use sort function from Arrays to sort an array.
    4)Use for loop and toString to print the the strings alphabetically ignore cases.
*/

import java.util.Arrays; 
public class SortAarray{
    public static void main(String[] args){
        String [] place = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(place);
        System.out.println(Arrays.toString(place));
        for(int i=0;i<place.length;i++)
        {
            if(i%2==0) {
                place[i] = place[i].toUpperCase();
            }
        }
        System.out.print(Arrays.toString(place));
    }
}