/*
Requirement:
    To change the given progarm in the form of compound assignments
Entities:
    ArithmeticDemo
Function Declaration:
    public static void main (String[] args)
Jobs to be done:
    Covert the operators into compound assignments in the given program and then run to get the output.
*/
 class ArithmeticDemo {

    public static void main (String[] args){
        int result = 1+2; // result is now 3
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
        System.out.println(result);
        result %= 7; // result is now 3
        System.out.println(result);

    }
}