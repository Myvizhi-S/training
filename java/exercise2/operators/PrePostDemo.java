/*Requirement:
    To explain why the value "6" is printed twice in a row in the following program:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }
Entities:
    PrePostDemo

Function declaration:
     No function declaration

Jobs to be done:
    1)Find the reason why the "6" is printed twice in a row.
*/
EXPLANATION:
    1) The class PrePostDemo is created.
    2) The variable i is declared and initialize the value as 3.
    3) The value is post incremented.(i=4)
    4) Then the Preincremented value of i is printed(Prints "4"),in this statement at firstb the i value is printed and the increment operation is done(i = 5).
    5) The i value is printed (prints "5").
    6) Then the preincremented value of "i" is printed, in this line the increment is done first and the incremented i value printed next(Prints "6")
    7) In next line the Postincremented value of i is printed so that the "i" valueprinted first and then the increment operation is done.(Prints "6").So that the value "6" is printed twice.
    8) Then the "i" value is printed.(prints "7").