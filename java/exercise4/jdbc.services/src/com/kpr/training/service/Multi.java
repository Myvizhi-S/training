package com.kpr.training.service;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCode;
import com.kpr.training.model.Address;
import com.kpr.training.model.Person;

public class Multi {
    
    
    
        private static final String COMMA_DELIMITER = ",";
         
    public static void readCsvFile(String fileName) {
 
        BufferedReader fileReader = null;
        try {
              
           ArrayList<Person> persons = new ArrayList<>();
            
             
            String line = "";
             
            fileReader = new BufferedReader(new FileReader(fileName));
            fileReader.readLine();
             
            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(COMMA_DELIMITER);
                if (tokens.length > 0) {
                    Address address = null;
                    Person person = new Person(tokens[0], tokens[1], tokens[2], dateValidator(tokens[3]));
                    if (tokens[4] != "NULL" && tokens[5] != "NULL" && Integer.parseInt(tokens[6]) != 0) {
                        address = new Address(tokens[4], tokens[5], Integer.parseInt(tokens[6]));
                    }
                    
                    person.setAddress(address);
                    persons.add(person);
                }
            }
             
            for (Person person : persons) {
                System.out.println(person.toString());
            }
        } 
        catch (Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader !!!");
                e.printStackTrace();
            }
        }
 
    }
     
    public static java.util.Date dateValidator(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date utilDate = null;
        boolean valid = false;
        
        try {
            LocalDate.parse(date,
                    DateTimeFormatter.ofPattern("dd-MM-uuuu")
                            .withResolverStyle(ResolverStyle.STRICT));
            
            valid = true;
        } catch (Exception e) {
            valid = false;
            throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
        }
        
        if (valid == true) {
            try {
                utilDate = formatter.parse(date);
            } catch (Exception e) {
                throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
            }
        }
        return utilDate;
    }

}
