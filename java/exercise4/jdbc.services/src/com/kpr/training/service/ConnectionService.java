/*
Requirement:
    To create a connection between the database and java application.
    
Entity:
    JdbcConnection
    
Function declaration:
    public void init() 
    public void release() 
    
Jobs to be done:
    
    1. Declare Connection con.
    2. Declare init method.
    	2.1 Load db.properties file to a properties object.
        2.2 Establish the connection using sql driver and store it as con.
    3. Declare get method.
        3.1 Return connection object.
    4. Declare release method.
        4.1 Close the Connection of the server.
        4.2 Close the PreparedStatement of the query.
    5. Declare commit method.
        5.1 Commit the changes made after execution of query.
    6. Declare rollback method.
        6.1 Rollback the changes made by executing the query.

Pseudo code:
class ConnectionService {
    
    public Connection con;

    public void init() {

        Properties properties = new Properties();
        
        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }
            
            con = DriverManager.getConnection(properties.getProperty("URL"),
                    properties.getProperty("User Name"), properties.getProperty("Password"));
            con.setAutoCommit(false);

        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS, e);
        } 

    }
    
    public static Connection get() {
        
        return con;
    }

    public void release() {
        try {
            con.close();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
    
    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }

    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
}

*/

package com.kpr.training.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import com.kpr.training.constant.Constant;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCode;

@SuppressWarnings("unused")
public class ConnectionService {

    private Connection con = null;
    
    public void init() {

        Properties properties = new Properties();

        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }
            
            con = DriverManager.getConnection(properties.getProperty(Constant.URL),
                    properties.getProperty(Constant.USER_NAME), properties.getProperty(Constant.PASSWORD));
            con.setAutoCommit(false);

        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS, e);
        } 
    }
    
    public Connection get() {
        return this.con;
    }
    
    public void release() {
        
        try {
            con.close();
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS_TO_CLOSE, e);
        }
    }

    public void commit() {
        
        try {
            con.commit();
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_COMMIT);
        }
    }
    
    public void rollback() {
        
        try {
            con.rollback();
        } catch (Exception e1) {
            throw new AppException(ErrorCode.FAILED_TO_ROLLBACK);
        }
    }
}

