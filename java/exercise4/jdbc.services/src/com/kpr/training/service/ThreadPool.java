/*
 * Requirement: 
 *       Getting connection from the client, decide what is the desirable place to get the connection.
 * 
 * Entity:
 *      ThreadPool
 * 
 * Method Signature:
 *      public void run() {}
 *      public void setConnection() {}
 *      public void createCon(ConnectionService connectionService) {}
 *      public static Connection get() {}
 *      public static void commit() {}
 *      public static void rollback() {}
 *      
 * Jobs To Be Done:
 *      1) Creating the object for ThreadLocal as local
 *      2) ConnectionService and thread objects are created.
 *      3) Checking whether the thread is not alive,
 *          3.1) If not alive, making the respective thread to run.
 *      4) Initializing the connections in connectionService using init() and linking those connections with running thread .
 *  
 * Pseudo Code:
 *      class ThreadPool {
 *          public static ThreadLocal<Connection> local = new ThreadLocal<>();
 *          private static Connection con;
    
            ConnectionService connection1 = new ConnectionService();
            ConnectionService connection2 = new ConnectionService();
            ConnectionService connection3 = new ConnectionService();
            
            public Thread thread1 = new Thread() {
                public void run() {
                    System.out.println("Thread 1 is running");
                    createCon(connection1);
                }
            };
            public Thread thread2 = new Thread() {
                public void run() {
                    System.out.println("Thread 2 is running");
                    createCon(connection2);
                }
            };
            public Thread thread3 = new Thread() {
                public void run() {
                    System.out.println("Thread 3 is running");
                    createCon(connection3);
                }
            };
            public void setConnection() {
        
                if (thread1.isAlive() == false) {
        
                    thread1.run();
                }
                else if (thread2.isAlive() == false) {
        
                    thread2.run();
                } else if (thread3.isAlive() == false) {
        
                    thread3.run();
                }
            }
            public void createCon(ConnectionService connectionService) {
                ConnectionService.init();
                local.set(ConnectionService.get());
                con = local.get();
                
            }
            public static Connection get() {
                //Creating Connection
                return con;
            }
            public static void commit() {
                //creating commit by handing exceptions
                get().commit();
            }
            public static void rollback() {
                //creating rollback by handling exceptions
               get().rollback();
            }
 *      }
 */

package com.kpr.training.service;

import java.sql.Connection;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCode;

public class ThreadPool {
    
    public static ThreadLocal<Connection> local = new ThreadLocal<>();
    
    ConnectionService connection1 = new ConnectionService();
    ConnectionService connection2 = new ConnectionService();
    ConnectionService connection3 = new ConnectionService();
    
    public Thread thread1 = new Thread() {
        public void run() {
            System.out.println("Thread 1 is running");
            createCon(connection1);
        }
    };
    public Thread thread2 = new Thread() {
        public void run() {
            System.out.println("Thread 2 is running");
            createCon(connection2);
        }
    };
    public Thread thread3 = new Thread() {
        public void run() {
            System.out.println("Thread 3 is running");
            createCon(connection3);
        }
    };
    
    public void setConnection() {
        
        if (thread1.isAlive() == false) {
//            thread1.start(); 
            thread1.run();
        }
        else if (thread2.isAlive() == false) {
//            thread2.start();
            thread2.run();
        } else if (thread3.isAlive() == false) {
//            thread3.start();
            thread3.run();
        }
    }
    
    public void createCon(ConnectionService connectionService) {
        connectionService.init();
        local.set(connectionService.get());
        System.out.println(local.get());
    }
    
    public static Connection get() {
        return local.get();
    }
    
    public static void commit() {
        
        try {
            local.get().commit();
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_COMMIT);
        }
    }
    
    public static void rollback() {
        
        try {
            local.get().rollback();
        } catch (Exception e1) {
            throw new AppException(ErrorCode.FAILED_TO_ROLLBACK);
        }
    }
    
    public static void release() {
        
        try {
            local.get().close();
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS_TO_CLOSE, e);
        }
    }
}
