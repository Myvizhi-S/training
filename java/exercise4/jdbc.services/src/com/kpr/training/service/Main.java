package com.kpr.training.service;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import com.kpr.training.constant.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCode;
import com.kpr.training.model.Address;
import com.kpr.training.model.Person;

public class Main {

    public static void main(String[] args) {
      AddressService service = new AddressService();
      ConnectionService.init();
      Address address1 = new Address("sdfddsf", "sfyhd", 83748923);
      PersonService pService = new PersonService();
      Person person = new Person("balaji92203401", "pa", "adsbfeenglfi@.com", pService.dateValidator("09-02-2001"));
      person.setAddress(address1);
      person.setId(3);
      long id = pService.create(person);
      System.out.println(id);
      person.setId(2);
      pService.update(person);
      pService.delete(7);
      System.out.println(pService.readAll());
//     Multi.readCsvFile("1.csv");
//      pService.delete(10);
//      System.out.println(pService.dateValidator("09-02-2001"));
//      ConnectionService.commit();
//      System.out.println(pService.read(11, true));
//      Date date = new Date(System.currentTimeMillis() - );  
//      SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
//      String strDate= formatter.format(date);  
//      System.out.println(strDate); 
    }

}
