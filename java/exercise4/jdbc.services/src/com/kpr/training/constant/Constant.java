package com.kpr.training.constant;

public class Constant {

    public static final String URL          = "URL";
    public static final String USER_NAME    = "User Name";
    public static final String PASSWORD     = "Password";
    public static final String ID           = "id";
    public static final String STREET       = "street";
    public static final String CITY         = "city";
    public static final String PINCODE      = "pincode";
    public static final String FIRSTNAME    = "firstName";
    public static final String LASTNAME     = "lastName";
    public static final String EMAIL        = "email";
    public static final String ADDRESS_ID   = "address_id";
    public static final String BIRTH_DATE   = "birth_date";
    public static final String CREATED_DATE = "created_date";
    
}
